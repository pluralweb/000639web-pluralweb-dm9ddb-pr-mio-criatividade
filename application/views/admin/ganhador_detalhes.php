    <div class="pageheader">
      <h2><i class="fa fa-list"></i> Detalhes de Cadastro</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Cadastros</li>
          <li><a href="<?php echo base_url('admin/cadastros/listagem') ?>">Listagem</a></li>
          <li class="active">Detalhes</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Ganhador</h3>
          <p>Visualize os dados completos de um ganhador.</p>
        </div>
            <div class="panel-body">

            <div class="row">
              <div class="col-sm-6">
                <strong>Imagem do mês</strong>
                <div class="img">
                  <img src="<?php echo base_url($cadastro[0]->img); ?>" alt="" />
                </div>
              </div>
              <div class="col-sm-6">
                <strong>Imagem da ideia</strong>
                <div class="img">
                  <img src="<?php echo base_url($cadastro[0]->imgLamp); ?>" alt="" />
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <strong>Cargo</strong>
                <p><?php echo $cadastro[0]->cargo;?></p>
              </div>
              <div class="col-sm-6">
                <strong>Ganhador do mês</strong>
                <p><?php echo $cadastro[0]->mes;?></p>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <strong>Nome</strong>
                <p><?php echo $cadastro[0]->exibicao;?></p>
              </div>
              <div class="col-sm-6">
                <strong>E-mail</strong>
                <p><?php echo $cadastro[0]->email;?></p>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
              <strong>Titulo</strong>
                <p><?php echo $cadastro[0]->titulo;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
              <strong>Qual a sua ideia?</strong>
                <p><?php echo $cadastro[0]->ideia;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <strong>Qual a sua contribuicao pessoal no projeto?</strong>
                <p><?php echo $cadastro[0]->contribuicao;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
              <strong>Quem pode atestar a sua participação?</strong>
                <p><?php echo $cadastro[0]->quempode;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
              <strong>Por que merece ganhar?</strong>
                <p><?php echo $cadastro[0]->merece;?></p>
              </div>
            </div>
            </div>
     </div>

     <div class="row">
        <div class="col-sm-6">
          <a href="javascript: window.history.go(-1)" class="btn btn-primary">Voltar</a>
          <form class="inlineBlock" action="<?php echo base_url('admin/ganhadores/editar'); ?>" method="post">
            <input type="hidden" name="idUser" value="<?php echo $this->uri->segment(4); ?>">
            <button type="submit" class="btn btn-primary">Editar</button>
          </form>
        </div>
     </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

</body>
</html>
