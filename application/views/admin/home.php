    <div class="pageheader">
      <h2><i class="fa fa-home"></i> Home</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li class="active">Home</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

    	<div class="row">

            <div class="col-sm-6 col-md-5">
              <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                  <div class="stat">
                    <div class="row">
                      <div class="col-xs-4">
                        <img src="<?php echo base_url('assets/images/is-user.png') ?>" alt="" />
                      </div>
                      <div class="col-xs-8">
                        <small class="stat-label">Total de cadastros</small>
                    	<h1><?php echo number_format($cadastros,0,',','.'); ?></h1>
                      </div>
                    </div><!-- row -->

                    <div class="mb15"></div>

                    <div class="row">
                      <div class="col-xs-4">
                        <small class="stat-label">Pendentes</small>
                        <h4><?php echo number_format($pendentes,0,',','.'); ?></h4>
                      </div>

                      <div class="col-xs-4">
                        <small class="stat-label">Shortlist</small>
                        <h4><?php echo number_format($aprovados,0,',','.'); ?></h4>
                      </div>

                      <div class="col-xs-4">
                        <small class="stat-label">Reprovadas</small>
                        <h4><?php echo number_format($reprovados,0,',','.'); ?></h4>
                      </div>
                    </div>

                  </div><!-- stat -->

                </div><!-- panel-heading -->
              </div><!-- panel -->
            </div><!-- col-sm-6 -->

        </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.tagsinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-timepicker.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";

	jQuery(".select2").select2({
	width: '100%',
	});

	jQuery('#data').datepicker();
	jQuery("#data").mask("99/99/9999");
	jQuery('#hora').timepicker({showMeridian: false});

	jQuery('#dataP').datepicker();
	jQuery("#dataP").mask("99/99/9999");
	jQuery('#horaP').timepicker({showMeridian: false});

	jQuery("#formPrevCat").submit(function(e) {
		e.preventDefault();

		var hotsite = jQuery("#hotsite").val();
		var categoria = jQuery("#categoria").val();
		var data = jQuery("#data").val();
		var hora = jQuery("#hora").val();

		if(hotsite && categoria && data && hora) {

			data = data.split("/");
			data = data[2]+'-'+data[1]+'-'+data[0];

			hora = hora.replace(":", "-");

			var url = '<?php echo base_url(); ?>'+hotsite+'/'+categoria+'/?preview=1&data='+data+'&hora='+hora;

			window.open(encodeURI(url), '_blank');

		}

	});

	jQuery("#formPrevProd").submit(function(e) {
		e.preventDefault();

		var hotsite = jQuery("#hotsiteP").val();
		var categoria = jQuery("#categoriaP").val();
		var produto = jQuery("#produtoP").val();
		var data = jQuery("#dataP").val();
		var hora = jQuery("#horaP").val();

		if(hotsite && categoria && produto && data && hora) {

			data = data.split("/");
			data = data[2]+'-'+data[1]+'-'+data[0];

			hora = hora.replace(":", "-");

			var url = '<?php echo base_url(); ?>'+hotsite+'/'+categoria+'/'+produto+'/?preview=1&data='+data+'&hora='+hora;

			window.open(encodeURI(url), '_blank');

		}

	});

	jQuery("#categoriaP").change(function() {

		var cat = jQuery(this).val();

		jQuery.post(base_url+'admin/home/getProdutosByCat', { cat: cat }, function(data) {
			if(data) {
				jQuery("#produtoP").html(data);
				jQuery("#produtoP").select2({
					width: '100%',
				});
			}
		});
	});

  });
</script>

</body>
</html>
