    <div class="pageheader">
      <h2><i class="fa fa-list"></i> Listagem de Cadastros Pendentes</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Cadastros Pendentes</li>
          <li class="active">Listagem</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Cadastros Pendentes</h3>
          <p>Esta lista mostra todos os cadastros realizados no site que ainda estão pendentes de aprovação.</p>
        </div>
        <div class="panel-body">
          <!-- table-responsive -->
          <div class="table-responsive">
          <form action="<?php echo base_url('admin/cadastros/aprovar/')?>" method="post">
              <?php
               function month($mes){
                switch ($mes){
                  case 1: $mes = "Janeiro"; break;
                  case 2: $mes = "Fevereiro"; break;
                  case 3: $mes = "Março"; break;
                  case 4: $mes = "Abril"; break;
                  case 5: $mes = "Maio"; break;
                  case 6: $mes = "Junho"; break;
                  case 7: $mes = "Julho"; break;
                  case 8: $mes = "Agosto"; break;
                  case 9: $mes = "Setembro"; break;
                  case 10: $mes = "Outubro"; break;
                  case 11: $mes = "Novembro"; break;
                  case 12: $mes = "Dezembro"; break;
                }
                return $mes;
               }
               ?>
               <?php date_default_timezone_set('America/Sao_Paulo'); ?>
          <table class="table table-striped" id="listagem">
              <thead>
                 <tr>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Qual a ideia?</th>
                    <th>Data de Cadastro</th>
                    <th>Avaliar</th>
                    <th>Ações</th>
                 </tr>
              </thead>
              <tbody>
                 <?php foreach($cadastros as $cad) {?>
                 <tr>
                    <td><?php echo $cad->nome;?></td>
                    <td><?php echo $cad->email;?></td>
                    <td><?php
                      $resumo = (strlen($cad->ideia) > 200) ? substr($cad->ideia, 0, strrpos(substr($cad->ideia, 0, 200), ' '))."..." : $cad->ideia;
                      echo $resumo;
                     ?></td>
                    <td data-order="<?php echo strtotime($cad->data_add); ?>"><?php echo date('d/m/Y h:i', strtotime($cad->data_add));?></td>
                    <td>
                      <div class="checkbox block">
                        <label style="padding-left: 0">
                          <button type="submit" href="javascript:void(0);" name="aprovacao[<?php echo $cad->idCampo; ?>]" class="btn btn-default" value="f">Finalista</button>
                        </label>
                      </div>
                      <div class="checkbox block">
                        <label style="padding-left: 0">
                          <button type="submit" href="javascript:void(0);" name="aprovacao[<?php echo $cad->idCampo; ?>]" class="btn btn-default" value="a">Shortlist</button>
                        </label>
                      </div>
                      <div class="checkbox block">
                        <label style="padding-left: 0">
                          <button type="submit" href="javascript:void(0);" name="aprovacao[<?php echo $cad->idCampo; ?>]" class="btn btn-default" value="r">Reprovar</button>
                        </label>
                      </div>
                  </td>
                    <td>
                      <a href="<?php echo base_url('admin/cadastros/detalhespendentes/'.$cad->idCampo)?>" title="Detalhes" class="btn btn-default btn"><i class="glyphicon glyphicon-search"></i></a>
					          </td>
                 </tr>
                 <?php } ?>
               </form>
              </tbody>
           </table>
           <div class="col-sm-4 col-sm-offset-8">
           <button type="submit" class="btn btn-primary btn-block">Aprovar/Reprovar Selecionadas</button>
           </div>
          </div><!-- table-responsive -->

        </div><!-- panel-body -->
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>
<style>
#layer2{width:100%; height:100vh; position:fixed; top:0; left:0; display:table !important; opacity:0; z-index:100}
#layer2 #overlay{width:100%; height:100%; position:absolute; top:0; left:0; opacity:0.8; background-color:#000}
#layer2 #content{width:100%; height:100%; display:table-cell; text-align:center; vertical-align:middle;}
#layer2 #content img{display:inline; max-width:90vw; max-height:90vh; border:22px solid #FFF; border-radius:22px; position:relative}
#layer2 #content .close{width:20px; height:20px; position:absolute; top:9px; right:11px; background:url(<?php echo base_url('imgs/closeBg2.png') ?>) center center no-repeat; z-index:11; opacity:1 !important;}


.modalImg{cursor:pointer; position:relative}
.modalImg:after{content:'\e105'; font:40px/47px 'Glyphicons Halflings'; color:#FFF; text-align:center; width:50px; height:50px; display:block; position:absolute; top:50%; left:50%; margin:-25px -0px -0px -25px; opacity:0}
.modalImg:hover:after{opacity:0.8}

</style>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/dataTables.scroller.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function($) {
    var url = "<?php echo base_url(); ?>";

    "use strict";

    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });

    //

    // Select2
    jQuery('select').select2({
    });

    jQuery('select').removeClass('form-control');

    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });

        return false;
    });

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });


  });
</script>

<script>
	$(document).ready(function(e) {
       $(".modalImg").click(function () {
			var target = $(this).data("image");
			var root   = $("body");
			var h = $('body').innerHeight();
			//console.log(target);
			var html   = "<div id='layer2' class='images'><div id='content'><div id='overlay'></div><img class='' src='"+target+"'><a href='' class='close'></a></div></div>";

			root.addClass("openLayer").append(html);

			$('#layer2').animate({opacity:1}, 1000);
			$("#layer2 .close, #layer2 #overlay").click(function () {
				$(this).closest("#layer2").fadeOut(1000, function () {
					$(this).remove();
				});
				$('body').removeClass("openLayer");
				return false;
			});
			return false;
		});
    });
</script>
</body>
</html>
