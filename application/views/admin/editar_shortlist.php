    <div class="pageheader">
      <h2><i class="fa fa-list"></i> Ganhador</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Ganhadores</li>
          <li><a href="<?php echo base_url('/usuarios/listagem') ?>">Listagem</a></li>
          <li class="active">Editar Ganhador</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">
      <div class="panel panel-default">
        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/cadastros/shortlistEdit') ?>" method="post" enctype="multipart/form-data">
          <input name="idUser" value="<?php echo $cadastro[0]->idUser;?>" type="hidden">
          <input name="idCampo" value="<?php echo $cadastro[0]->idCampo;?>" type="hidden">
          <input name="idGanhador" value="<?php echo $cadastro[0]->idGanhador;?>" type="hidden">
          <div class="panel-body panel-body-nopadding">
            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label">Deixe em branco caso não deseje alterar</label>
            </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label">Nome de exibição</label>
                <input type="text" name="exibicao" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <?php
               function month($mes){
            		switch ($mes){
            			case 1: $mes = "Janeiro"; break;
            			case 2: $mes = "Fevereiro"; break;
            			case 3: $mes = "Março"; break;
            			case 4: $mes = "Abril"; break;
            			case 5: $mes = "Maio"; break;
            			case 6: $mes = "Junho"; break;
            			case 7: $mes = "Julho"; break;
            			case 8: $mes = "Agosto"; break;
            			case 9: $mes = "Setembro"; break;
            			case 10: $mes = "Outubro"; break;
            			case 11: $mes = "Novembro"; break;
            			case 12: $mes = "Dezembro"; break;
            		}
            		return $mes;
            	 }
               ?>

              <div class="col-sm-6">
                <label class="control-label">Shortlist do mês</label>
                <select class="input-sm col-sm-12" name="mes" required>
                  <option value="a">Selecione um mes</option>
                  <?php for ($i= $datas[0]->inicio; $i < $datas[0]->fim; $i++) {
                  $sel = ($cadastro[0]->mes == $i) ? selected : "";
                  echo '<option value="'.$i.'" '.$sel.' >'.month($i).'</option>';
                } ?>
                </select>
              </div>
              <div class="clearfix"></div>
            </div>

          </div><!-- panel-body -->

          <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </div>
               </div>
            </div>
        </form>

      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";



  });
</script>

</body>
</html>
