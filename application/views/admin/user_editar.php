    <div class="pageheader">
      <h2><i class="fa fa-pencil"></i> Inserir Usuários do site</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade </a></li>
          <li>Usuários do site</li>
          <li class="active"> Editar usuário</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Usuário do site</h3>
          <p>Aqui você edita um cadastro de usuário para o site.</p>
        </div>

        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/user/edt') ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="panel-body panel-body-nopadding">

            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label" for="nome_br">Nome</label>
                <input type="text" id="nome" name="nome" class="form-control" value="<?php echo $mentor->nome; ?>" />
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label" for="nome_br">E-mail</label>
                <input type="text" id="nome" name="email" value="<?php echo $mentor->email; ?>" class="form-control" />
              </div>
            </div>

          </div><!-- panel-body -->

          <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                  </div>
               </div>
            </div>

        </form>
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.tagsinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/colorpicker.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/wysihtml5-0.3.0.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-wysihtml5.js') ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<style media="screen">
  img{
    width: 100%;
  }
</style>

<script>
  $(document).ready(function($) {

    "use strict";

	$(".select2").select2({width: '100%'});

  $(document).on('click',".remover", function(e) {
    e.preventDefault();
    var el = $(this);
    var qtde = el.closest('#interesses').find('.relac').length;
    if(qtde>1) {
      $(this).closest('.relac').remove();
    }
  });
  $("#addInt").click(function(e) {
    e.preventDefault();
    var clone = $('#interesses .relac:first').clone();
    $('#interesses').append('<div class="row relac"><div class="col-sm-5"><input type="text" name="interesse[]" class="form-control" /></div><div class="col-sm-5"><select name="tipo[]" data-placeholder="Selecione" title="Selecione a opção" class="select2 cenarios_rel"><option>Selecione o interesse...</option><option value="1">Livros</option><option value="2">Filmes</option><option value="3">Cantores</option></select></div><div class="col-sm-2"><a href="#" title="Remover" class="btn btn-default btn remover"><i class="glyphicon glyphicon-trash"></i></a></div><br /><br /></div>').find('select').select2({width: '100%'});
  });

	$('.wycs').wysihtml5({
		"font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
		"emphasis": true, //Italics, bold, etc. Default true
		"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
		"html": true, //Button which allows you to edit the generated HTML. Default false
		"link": false, //Button to insert a link. Default true
		"image": false //Button to insert an image. Default true,
	});

  // Color Picker
  if($('#cor').length > 0) {
     $('#colorSelector').ColorPicker({
        onShow: function (colpkr) {
          $(colpkr).fadeIn(500);
          return false;
        },
        onHide: function (colpkr) {
          $(colpkr).fadeOut(500);
          return false;
        },
        onChange: function (hsb, hex, rgb) {
          $('#colorSelector span').css('backgroundColor', '#' + hex);
          $('#cor').val('#'+hex);
        }
     });
  }
  });

</script>

</body>
</html>
