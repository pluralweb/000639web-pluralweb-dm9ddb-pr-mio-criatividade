    <div class="pageheader">
      <h2><i class="fa fa-user"></i> Listagem de Usuários</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Usuários</li>
          <li class="active">Listagem</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Usuários</h3>
          <p>Esta lista mostra todos os usuários que estão cadastrados no sistema.</p>
        </div>
        <div class="panel-body">
          <!-- table-responsive -->
          <div class="table-responsive">
          <table class="table table-striped" id="listagem">
              <thead>
                 <tr>
                    <th>Nome</th>
                    <th>Usuário</th>
                    <th>Permissões</th>
                    <th>Ações</th>
                 </tr>
              </thead>
              <tbody>
                 <?php foreach($usuarios as $user) {?>
                 <tr>
                    <td><?php echo $user->nome;?></td>
                    <td><?php echo $user->usuario;?></td>
                    <td><?php 
					if(isset($user->permissoes)) {
						foreach($user->permissoes as $perm) {
							echo $perm->nome."<br>";
						}
					}?></td>
                    <td>
                      <a href="<?php echo base_url('admin/usuarios/editar/'.$user->id)?>" title="Editar" class="btn btn-default btn"><i class="glyphicon glyphicon-edit"></i></a>
                      <a href="<?php echo base_url('admin/usuarios/excluir/'.$user->id)?>" title="Excluir" class="btn btn-default btn"><i class="glyphicon glyphicon-remove"></i></a>
					</td>
                 </tr>
                 <?php } ?>
              </tbody>
           </table>
          </div><!-- table-responsive -->
          
        </div><!-- panel-body -->
      </div>
      
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {
    
    "use strict";
    
    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });
    
    // Select2
    jQuery('select').select2({
    });
    
    jQuery('select').removeClass('form-control');
    
    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });
        
        return false;
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>

</body>
</html>
