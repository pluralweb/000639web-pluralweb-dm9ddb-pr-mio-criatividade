    <div class="pageheader">
      <h2><i class="fa fa-star"></i> Configurações</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Configurações</li>
          <li class="active">Cadastro</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Inicio</h3>
        </div>
        <div class="panel-body">
          <!-- table-responsive -->
          <?php if($cadastros){ ?>
          <div class="table-responsive">
            <form action="<?php echo base_url('admin/configuracao/deleteMes'); ?>" method="post">
              <table class="table table-striped" id="listagem">
              <thead>
                 <tr>
                    <th>Inicio do concurso</th>
                    <th>Término do concurso</th>
                    <th>Ações</th>
                 </tr>
              </thead>
              <tbody>
                 <?php foreach($cadastros as $cad) { ?>
                 <tr>
                    <td><?php echo $cad->inicio;?></td>
                    <td><?php echo $cad->fim;?></td>
                    <td>
                      <button type="submit" name="delete" class="btn btn-default" value="<?php echo $cad->id;?>">Excluir</button>
					          </td>
                 </tr>
                 <?php } ?>
               </tbody>
              </table>
            </form>
          </div><!-- table-responsive -->
          <?php } ?>
          <?php if($hideForm){ ?>
          <form class="form-horizontal form-bordered inputMonth" action="<?php echo base_url('admin/configuracao/savemes') ?>" method="post" enctype="multipart/form-data">
              <div class="panel-body panel-body-nopadding">

              <div class="form-group">
                <div class="col-sm-6">
                    <label class="control-label">Início do Concurso</label>
                        <select class="input-sm col-sm-12 begin" name="incio">
                          <?php foreach ($datas as $key => $value) {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                          } ?>
                        </select>
                </div>
                <div class="col-sm-6">
                    <label class="control-label">Fim do Concurso</label>
                    <select class="input-sm col-sm-12 end" name="incio">
                      <?php foreach ($datas as $key => $value) {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                      } ?>
                    </select>
                </div>
              </div>

            </div><!-- panel-body -->

            <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                  </div>
               </div>
            </div>

          </form>
          <?php } ?>

        </div><!-- panel-body -->
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";


    $('.inputMonth').submit(function(e){
      e.preventDefault();
      var urlAction = $(this).attr('action');
      var el = $(this);
      var begin = parseInt(el.find('.begin option:selected').val());
      var end = parseInt(el.find('.end option:selected').val());

      if(end > begin){
        var obj = {};
        obj.inicio = begin;
        obj.fim = end;

        $.post(urlAction, obj , function(){
          location.reload();
        })
      }else{
        alert('O fim do concurso deve ser após o início');
      }
    })

    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });

    // Select2
    jQuery('select').select2({
    });

    jQuery('select').removeClass('form-control');

    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });

        return false;
    });

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });


  });
</script>

</body>
</html>
