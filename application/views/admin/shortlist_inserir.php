    <div class="pageheader">
      <h2><i class="fa fa-trophy"></i> Inserir Ganhador</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Ganhadores</li>
          <li class="active">Inserir Ganhador</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Shortlist</h3>
          <p>Aqui você cadastra os dados da Shortlist no sistema.</p>
        </div>

        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/cadastros/shortlistSave') ?>" method="post" enctype="multipart/form-data">
            <div class="panel-body panel-body-nopadding">
            <div class="form-group">
              <input type="hidden" name="idUser" value="<?php echo $idUser->idUser; ?>">
              <input type="hidden" name="idCampo" value="<?php echo $idCampo; ?>">
              <div class="col-sm-6">
                  <label class="control-label">Nome de exibição</label>
                  <input type="text" name="exibicao" class="form-control" required />
              </div>
                  <?php
                   function month($mes){
                		switch ($mes){
                			case 1: $mes = "Janeiro"; break;
                			case 2: $mes = "Fevereiro"; break;
                			case 3: $mes = "Março"; break;
                			case 4: $mes = "Abril"; break;
                			case 5: $mes = "Maio"; break;
                			case 6: $mes = "Junho"; break;
                			case 7: $mes = "Julho"; break;
                			case 8: $mes = "Agosto"; break;
                			case 9: $mes = "Setembro"; break;
                			case 10: $mes = "Outubro"; break;
                			case 11: $mes = "Novembro"; break;
                			case 12: $mes = "Dezembro"; break;
                		}
                		return $mes;
                	 }
                   ?>
                  <div class="col-sm-6">
                    <label class="control-label">Shortlist do mês</label>
                    <select class="input-sm col-sm-12" name="mes">
                      <option value="a">Selecione um mes</option>
                      <?php for ($i= $datas[0]->inicio; $i < $datas[0]->fim; $i++) {
                      echo '<option value="'.$i.'">'.month($i).'</option>';
                    } ?>
                    </select>
              </div>
              <div class="clearfix">

              </div>
            </div>

          </div><!-- panel-body -->

          <div class="panel-footer">
             <div class="row">
                <div class="col-sm-6">
                  <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
             </div>
          </div>

        </form>
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";

	jQuery(".select2").select2({
		width: '100%',
	});

  });
</script>

</body>
</html>
