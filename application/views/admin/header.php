<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png') ?>" type="image/png">

  		<title>Prêmio Criatividade</title>

        <link href="<?php echo base_url('assets/css/style.default.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/style.katniss.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/jquery.datatables.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/scroller.dataTables.min.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/jquery.tagsinput.css') ?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/colorpicker.css') ?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/bootstrap-editable.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/font.roboto.css'); ?>" rel="stylesheet">
  		<link href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css'); ?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/css/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/html5shiv.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/respond.min.js') ?>"></script>
        <![endif]-->
        <script> var base_url = "<?php echo base_url(); ?>"</script>

    </head>

    <body>
        <!-- Preloader -->
        <div id="preloader">
            <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
        </div>

        <section>

            <div class="leftpanel">

                <div class="logopanel">
                    <h1><span>[</span>Prêmio Criatividade<span>]</span></h1>
                </div><!-- logopanel -->

                <div class="leftpanelinner">

                    <!-- This is only visible to small devices -->
                    <div class="visible-xs hidden-sm hidden-md hidden-lg">
                        <div class="media userlogged">
                            <img alt="" src="<?php echo base_url('assets/images/photos/user1.png') ?>" class="media-object">
                            <div class="media-body">
                                <h4><?php echo $nome; ?></h4>
                            </div>
                        </div>

                        <h5 class="sidebartitle actitle">Conta</h5>
                        <ul class="nav nav-pills nav-stacked nav-bracket mb30">
                            <li><a href="<?php echo base_url('admin/home/perfil') ?>"><i class="fa fa-user"></i> <span>Perfil</span></a></li>
                            <li><a href="<?php echo base_url('admin/home/logout') ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </div>

                    <h5 class="sidebartitle">Menu</h5>
                    <ul class="nav nav-pills nav-stacked nav-bracket">
                        <li<?php if ($sessao == 'home') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/') ?>"><i class="fa fa-home"></i> <span>Home</span></a></li>

                        <?php if (in_array(5, $permissoes)) { ?>
                            <li class="<?php if ($sessao == 'cadastros') { ?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-list"></i> <span>Cadastros</span></a>
                                <ul class="children"<?php if ($sessao == 'cadastros') { ?> style="display:block"<?php } ?>>
                                    <li<?php if ($sessao == 'cadastros' && $subsessao == 'pendentes') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/cadastros/pendentes') ?>"><i class="fa fa-caret-right"></i> Pendentes</a></li>
                                    <li<?php if ($sessao == 'cadastros' && $subsessao == 'destaque') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/destaque') ?>"><i class="fa fa-caret-right"></i> Shortlist</a></li>
                                    <li<?php if ($sessao == 'cadastros' && $subsessao == 'finalista') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/ganhadores') ?>"><i class="fa fa-caret-right"></i> Finalistas</a></li>
                                    <li<?php if ($sessao == 'cadastros' && $subsessao == 'reprovados') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/cadastros/reprovados') ?>"><i class="fa fa-caret-right"></i> Reprovados</a></li>
                                    <li<?php if ($sessao == 'exportar' && $subsessao == 'todos') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/cadastros/exportAll') ?>"><i class="fa fa-caret-right"></i> Exportar</a></li>

                                </ul>
                            </li>
                        <?php } ?>

                        <?php if (in_array(6, $permissoes)) { ?>
                            <li class="<?php if ($sessao == 'configuracao') { ?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-gear"></i> <span>Configurações</span></a>
                                <ul class="children"<?php if ($sessao == 'configuracao') { ?> style="display:block"<?php } ?>>
                                    <li<?php if ($sessao == 'configuracao' && $subsessao == 'inscricao') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/configuracao/inscricao') ?>"><i class="fa fa-caret-right"></i> Inscrições</a></li>
                                    <li<?php if ($sessao == 'configuracao' && $subsessao == 'meses') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/configuracao/meses') ?>"><i class="fa fa-caret-right"></i> Meses</a></li>
                                </ul>
                            </li>
                        <?php } ?>

                        <?php if (in_array(6, $permissoes)) { ?>
                                                    <li class="<?php if ($sessao == 'mentores') { ?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-list"></i> <span>Usuários do Site</span></a>
                                                        <ul class="children"<?php if ($sessao == 'mentores') { ?> style="display:block"<?php } ?>>
                                                            <li<?php if ($sessao == 'mentores' && $subsessao == 'listagem') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/user/listagem') ?>"><i class="fa fa-caret-right"></i> Listagem</a></li>
                                                            <?php if (!$tipo): ?>
                                                              <li<?php if ($sessao == 'mentores' && $subsessao == 'inserir') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/user/inserir') ?>"><i class="fa fa-caret-right"></i> Inserir</a></li>
                                                              <?php /*
                                                              <li><a href="<?php echo base_url('admin/mentores/exportar') ?>"><i class="fa fa-caret-right"></i> Exportar</a></li>
                                                              */ ?>
                                                            <?php endif; ?>

                                                        </ul>
                                                    </li>
                                                <?php } ?>

                        <?php if (in_array(6, $permissoes)) { ?>
                            <li class="<?php if ($sessao == 'usuarios') { ?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-user"></i> <span>Usuários</span></a>
                                <ul class="children"<?php if ($sessao == 'usuarios') { ?> style="display:block"<?php } ?>>
                                    <li<?php if ($sessao == 'usuarios' && $subsessao == 'listagem') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/usuarios/index') ?>"><i class="fa fa-caret-right"></i> Ver Todos</a></li>
                                    <li<?php if ($sessao == 'usuarios' && $subsessao == 'inserir') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/usuarios/inserir') ?>"><i class="fa fa-caret-right"></i> Inserir</a></li>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>

                </div><!-- leftpanelinner -->
            </div><!-- leftpanel -->

            <div class="mainpanel">

                <div class="headerbar">

                    <a class="menutoggle"><i class="fa fa-bars"></i></a>

                    <div class="header-right">
                        <ul class="headermenu">
                            <li>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <img src="<?php echo base_url('assets/images/photos/user1.png') ?>" alt="" />
                                        <?php echo $nome; ?>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                        <li><a href="<?php echo base_url('admin/home/perfil') ?>"><i class="glyphicon glyphicon-user"></i> Meu Perfil</a></li>
                                        <li><a href="<?php echo base_url('admin/home/logout') ?>"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div><!-- header-right -->

                </div><!-- headerbar -->
