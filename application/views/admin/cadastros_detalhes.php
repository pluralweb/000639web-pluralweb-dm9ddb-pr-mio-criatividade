    <div class="pageheader">
      <h2><i class="fa fa-list"></i> Detalhes de Cadastro</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Cadastros</li>
          <li><a href="<?php echo base_url('admin/cadastros/listagem') ?>">Listagem</a></li>
          <li class="active">Detalhes</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Cadastros</h3>
          <p>Visualize os dados completos de um cadastro.</p>
        </div>
            <div class="panel-body">
            <div class="row">
              <div class="col-sm-6">
                <strong>Nome</strong>
                <p><?php echo $cadastro[0]->nome;?></p>
              </div>
              <div class="col-sm-6">
                <strong>E-mail</strong>
                <p><?php echo $cadastro[0]->email;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <?php
                 function month($mes){
                  switch ($mes){
                    case 1: $mes = "Janeiro"; break;
                    case 2: $mes = "Fevereiro"; break;
                    case 3: $mes = "Março"; break;
                    case 4: $mes = "Abril"; break;
                    case 5: $mes = "Maio"; break;
                    case 6: $mes = "Junho"; break;
                    case 7: $mes = "Julho"; break;
                    case 8: $mes = "Agosto"; break;
                    case 9: $mes = "Setembro"; break;
                    case 10: $mes = "Outubro"; break;
                    case 11: $mes = "Novembro"; break;
                    case 12: $mes = "Dezembro"; break;
                  }
                  return $mes;
                 }
                 ?>
                <strong>Shortlist do mês</strong>
                <p><?php echo month($cadastro[0]->mes);?></p>
              </div>
            </div>
            <?php if($cadastro[0]->ideia && $cadastro[0]->contribuicao && $cadastro[0]->quempode && $cadastro[0]->merece){ ?>
            <div class="row">
              <div class="col-sm-12">
                <strong>Qual a sua ideia?</strong>
                <p><?php echo $cadastro[0]->ideia;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <strong>Qual a sua contribuicao pessoal no projeto?</strong>
                <p><?php echo $cadastro[0]->contribuicao;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <strong>Quem pode atestar a sua participação?</strong>
                <p><?php echo $cadastro[0]->quempode;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
              <strong>Por que merece ganhar?</strong>
                <p><?php echo $cadastro[0]->merece;?></p>
              </div>
            </div>
            <?php } ?>
            </div>
            <?php if($destaque < 3){ ?>
              <form action="<?php echo base_url('admin/cadastros/aprovar/')?>" method="post" class="panel-heading">
                <strong>Shortlist?</strong>
                <div class="row">
                  <div class="checkbox inlineBlock col-sm-4">
                    <label style="padding-left: 0">
                      <input type="radio" name="aprovacao[<?php echo $cadastro[0]->idCampo; ?>]" value="a">
                      Aprovar
                    </label>
                  </div>
                  <div class="checkbox inlineBlock col-sm-4">
                    <label style="padding-left: 0">
                      <input type="radio" name="aprovacao[<?php echo $cadastro[0]->idCampo; ?>]" value="r">
                      Reprovar
                    </label>
                  </div>
                  <div class="checkbox inlineBlock col-sm-4">
                    <label style="padding-left: 0">
                      <input type="radio" name="aprovacao[<?php echo $cadastro[0]->idCampo; ?>]" value="d">
                      Desmarcar
                    </label>
                  </div>
                </div>
              </form>
              <?php } ?>
            <?php if($finalista == 0){ ?>
              <form action="<?php echo base_url('admin/cadastros/finalista/')?>" method="post" class="panel-heading">
                <strong>Seleção final?</strong>
                <div class="row">
                  <?php if($finalista !== 1){ ?>
                  <div class="checkbox inlineBlock col-sm-6">
                    <label style="padding-left: 0">
                      <input type="radio" name="aprovacao[<?php echo $cadastro[0]->idCampo; ?>]" value="f">
                      Ganhador
                    </label>
                  </div>
                  <?php } ?>
                </div>
              </form>
              <?php } ?>

     </div>

     <div class="row">
        <div class="col-sm-6">
          <a href="javascript: window.history.go(-1)" class="btn btn-primary">Voltar</a>
        </div>
     </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

</body>
</html>
