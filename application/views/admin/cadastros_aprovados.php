    <div class="pageheader">
      <h2><i class="fa fa-list"></i> Shortlist</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Shortlist</li>
          <li class="active">Listagem</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Shortlist</h3>
          <p>Esta lista mostra todos os cadastros realizados no site que tiveram sua participação aprovada.</p>
          <a href="<?php echo base_url('admin/cadastros/exportarShortlist') ?>" class="btn btn-sm btn-primary">Exportar Excel</a>
        </div>
        <div class="panel-body">
          <!-- table-responsive -->
          <div class="table-responsive">
          <table class="table table-striped" id="listagem">
              <thead>
                <tr>
                   <th>Nome</th>
                   <th>E-mail</th>
                   <th>Mês da shortlist</th>
                   <th>Qual sua ideia?</th>
                   <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                 <?php foreach($cadastros as $cad) {?>

                 <tr>
                    <td><?php echo $cad->exibicao;?></td>
                    <td><?php echo $cad->email;?></td>
                    <td><?php echo $cad->mes;?></td>
                    <td><?php
                      $resumo = (strlen($cad->ideia) > 200) ? substr($cad->ideia, 0, strrpos(substr($cad->ideia, 0, 200), ' '))."..." : $cad->ideia;
                      echo $resumo;
                     ?></td>
                    <td>
                      <a href="<?php echo base_url('admin/cadastros/detalhes/'.$cad->idCampo)?>" title="Detalhes" class="btn btn-default btn"><i class="glyphicon glyphicon-search"></i></a>
                      <a href="<?php echo base_url('admin/ganhadores/editarShortlist/'.$cad->idCampo)?>" title="Editar" class="btn btn-default btn"><i class="glyphicon glyphicon-edit"></i></a>
                    </td>
                 </tr>
                 <?php } ?>
              </tbody>

           </table>
          </div><!-- table-responsive -->

        </div><!-- panel-body -->
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";

    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });

    // Select2
    jQuery('select').select2({
    });

    jQuery('select').removeClass('form-control');

    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });

        return false;
    });

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });


  });
</script>

</body>
</html>
