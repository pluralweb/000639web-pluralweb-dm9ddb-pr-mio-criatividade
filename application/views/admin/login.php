<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png') ?>" type="image/png">

  <title>Prêmio Criatividade</title>

  <link href="<?php echo base_url('assets/css/style.default.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/font.roboto.css'); ?>" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url('assets/js/html5shiv.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/respond.min.js') ?>"></script>
  <![endif]-->
</head>

<body class="signin">


<section>

    <div class="signinpanel">

        <div class="row">

            <div class="col-md-6 col-md-offset-3">

                <div class="signin-info">
                    <div class="logopanel">
                        <h1><span>[</span> Prêmio Criatividade <span>]</span></h1>
                    </div><!-- logopanel -->

                    <div class="mb20"></div>
                </div><!-- signin0-info -->

            </div><!-- col-sm-7 -->

            <div class="col-md-6 col-md-offset-3">

                <?php echo form_open( 'admin/home/dologin' ); ?>
                    <h4 class="nomargin">Login</h4>
                    <p class="mt5 mb20">Faça login para acessar a sua conta.</p>

                    <input type="text" class="form-control uname" placeholder="Usuário" name="login" required />
                    <input type="password" class="form-control pword" placeholder="Senha" name="senha" required />
                    <button class="btn btn-success btn-block">Entrar</button><br />
                    <?php if(isset($invalid)) {?>
                    <div class="alert alert-danger">
                      Dados inválidos.
                    </div>
                    <?php } ?>

                <?php echo form_close(); ?>
            </div><!-- col-sm-5 -->

        </div><!-- row -->

        <div class="signup-footer">
            <div class="pull-left">
                &copy; 2016. Todos os direitos reservados
            </div>
            <div class="pull-right">
                Por: <a href="http://pluralweb.biz/" target="_blank">PluralWeb</a>
            </div>
        </div>

    </div><!-- signin -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>

<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

</body>
</html>
