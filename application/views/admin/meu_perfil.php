    <div class="pageheader">
      <h2><i class="fa fa-user"></i> Meu Perfil</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Home</li>
          <li class="active">Meu Perfil</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
	  <?php if($sucesso==1) {?>
      <div class="alert alert-success">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        Seu perfil foi atualizado com sucesso.
      </div>
      <?php } ?>
      
      <div class="panel panel-default">
      
        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/home/edtUsuario') ?>" method="post">
          <div class="panel-body panel-body-nopadding">
          
            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label">Nome Completo</label>
                <input type="text" name="nome" id="nome" value="<?php echo $usuario->nome;?>" class="form-control" required />
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label">Login </label>
                <input type="text" name="usuario" id="usuario" value="<?php echo $usuario->usuario;?>" class="form-control" required />
              </div>
              <div class="col-sm-6">
                <label class="control-label">Senha <small>(Deixe em branco caso não deseje alterar)</small></label>
                <input type="password" name="senha" id="senha" class="form-control">
              </div>
            </div>
            
          
          </div><!-- panel-body -->
          
          <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Salvar</button>&nbsp;
                    <button type="reset" class="btn btn-default">Cancelar</button>
                  </div>
               </div>
            </div>
        </form>

      </div>
      
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {
    
    "use strict";
    
  
  
  });
</script>

</body>
</html>
