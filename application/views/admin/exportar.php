    <div class="pageheader">
      <h2><i class="fa fa-star"></i> Configurações</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Prêmio Criatividade</a></li>
          <li>Configurações</li>
          <li class="active">Cadastro</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Inicio</h3>
        </div>
        <div class="panel-body">
          <!-- table-responsive -->
          <form class="form-horizontal form-bordered inputMonth" action="<?php echo base_url('admin/cadastros/exportarTodos') ?>" method="post">
              <div class="panel-body panel-body-nopadding">

              <div class="form-group">
                <div class="col-sm-6">
                    <label class="control-label">Início</label>
                        <input type="text" name="inicio" id="inicio" class="form-control datepicker" required="">
                </div>
                <div class="col-sm-6">
                    <label class="control-label">Fim</label>
                    <input type="text" name="fim" id="fim" class="form-control datepicker" required="">
                </div>
              </div>

            </div><!-- panel-body -->

            <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Exportar</button>
                  </div>
               </div>
            </div>

          </form>

        </div><!-- panel-body -->
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";
    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });

    // Select2
    jQuery('select').select2({
    });

    jQuery('select').removeClass('form-control');

    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });

        return false;
    });

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });


  });
</script>

</body>
</html>
