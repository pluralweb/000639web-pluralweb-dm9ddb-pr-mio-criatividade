<?php if($logged){ ?>
<section class="pre-footer">
  <button class="backtotop">Voltar para o topo</button>
  <a href="javascript:void(0);" title="DM9DDB" class="dm9ddb"></a>
  <a href="javascript:void(0);" title="Heróis da Criatividade0" class="herois"></a>
</section>
<?php } ?>
<?php if($month['valid']){ ?>
<footer>
    <a href="javascript:void(0);" title="Inscreva-se">
        <h5><span>Inscrições de <?php echo $month['begin']; ?><br />estão abertas.</span></h5>
        <h6><span>Participe<br />até <?php echo $month['end']; ?>.</span></h6>
    </a>
</footer>
<?php } ?>
</div>
<!-- .container -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="<?php echo base_url("js/plugins.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("js/functions.js"); ?>" type="text/javascript"></script>
</body>

</html>
