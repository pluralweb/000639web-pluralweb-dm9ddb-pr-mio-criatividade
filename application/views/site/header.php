<!doctype html>
<html>

    <head>
        <meta charset="utf-8" />
        <meta lang="pt-br" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Heróis da Criatividade</title>
        <link rel="shortcut icon" href="<?php echo base_url("imgs/favicon.png"); ?>" type="image/png">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,800italic,400italic,600,600italic,700,700italic,800|Passion+One:400,700,900' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("css/style.css"); ?>" />

        <meta name="description" content="Heróis da Criatividade" />

        <!-- Código para Google Authorship e Publisher -->
        <link rel="author" href="https://plus.google.com/(Google+_Profile)/posts"/>
        <link rel="publisher" href="https://plus.google.com/(Google+_Page_Profile)"/>

        <!-- Código do Schema.org também para o Google+ -->
        <meta itemprop="name" content="Heróis da Criatividade">
        <meta itemprop="description" content="Heróis da Criatividade">
        <meta itemprop="image" content="h<?php echo base_url(); ?>/image.jpg">

        <!-- para o Twitter Card -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="Conta do Twitter do site (incluindo arroba)">
        <meta name="twitter:title" content="Heróis da Criatividade">
        <meta name="twitter:description" content="Heróis da Criatividade">
        <meta name="twitter:creator" content="Conta do Twitter do autor do texto (incluindo arroba)">

        <!-- imagens largas para o Twitter Summary Card precisam ter pelo menos 280x150px -->
        <meta name="twitter:image" content="http://www.example.com/image.jpg">

        <!-- para o sistema Open Graph -->
        <meta property="og:title" content="Heróis da Criatividade" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="<?php echo base_url(); ?>" />
        <meta property="og:image" content="<?php echo base_url("image.jpg"); ?>" />
        <meta property="og:description" content="Heróis da Criatividade" />
        <meta property="og:site_name" content="Nome do site" />
        <meta property="article:published_time" content="2013-09-17T05:59:00+01:00" />
        <meta property="article:modified_time" content="2013-09-16T19:08:47+01:00" />
        <meta property="article:section" content="Seção do artigo" />
        <meta property="article:tag" content="Tags do artigo" />
        <meta property="fb:admins" content="Facebook numeric ID" />

        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#ffcb06">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#ffcb06">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="#ffcb06">
        <script type="text/javascript">
          var base_url = "<?php echo base_url(); ?>";
        </script>
    </head>
    <body>
        <div class="container">
            <header>
                <span></span>
                <!-- logo DM9DBB -->
            </header>


            <section class="login">
                <span class="login"></span>
                <!-- logo Heróis da Criatividade -->
                <h3>Prove que você tem<br />a criatividade em seu DNA.</h3>
                <p>O Heróis da Criatividade é uma iniciativa que irá recompensar com prêmios de até R$ 5.000,00 ideias que contribuam com a evolução da DM9DDB. <strong>Inscreva sua ideia e participe.</strong></p>
                <?php if(!$logged){ ?>
                <form id="login" action="" method="post">
                    <input type="text" name="email" class="email" placeholder="Digite seu e-mail DM9" />
                    <input type="submit" value="> Login" />
                    <span class="errormsg">E-mail incorreto<br />Digite corretamente seu e-mail DM9</span>
                </form>

                <span class="loginmsg"><a href="javascript:void(0);" class="u doLogin">Faça Login</a> para enviar sua ideia.</span>

                <?php } ?>

                <?php if($logged){ ?>

                <span class="welcome"><strong>Olá, </strong><?php echo $email; ?></span>

                <form id="logout" action="<?php echo base_url('home/logout') ?>" method="post">
                  <input type="submit" value="Sair">
                </form>

                <?php } ?>
            </section>
