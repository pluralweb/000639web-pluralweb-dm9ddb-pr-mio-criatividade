<?php if(!is_null($destaque)){ ?>
  <section class="ideiadomes">
      <div class="bg-responsive"></div>
      <div class="ideiadomes_img">
          <div>
              <span class="light light-1"></span>
              <span class="light light-2"></span>
              <span class="light light-3"></span>
              <span class="light light-4"></span>
              <span class="light light-5"></span>
              <span class="light light-6"></span>
              <span class="light light-7"></span>
          </div>
          <img class="imgReplace" src="<?php echo base_url($destaque[0]->imgLamp); ?>" alt="Ideia do Mês" />
          <div class="subtitle">
            <?php if($destaque[0]->exibicao){
              echo '<span>'. $destaque[0]->exibicao .'</span>';
            } ?>
            <?php if($destaque[0]->cargo){
              echo '<span class="small">'. $destaque[0]->cargo .'</span>';
            } ?>
          </div>
      </div>
      <article>
          <h3>Ideia de <?php echo $destaque[0]->mes; ?></h3>
          <h2><?php echo $destaque[0]->titulo; ?></h2>
          <p><?php echo $destaque[0]->ideia; ?></p>
      </article>
      <div class="clearfix"></div>
  </section>
<?php } ?>
<?php if($shortlist){ ?>
<section class="shortlist">
    <div class="shortlist-titles">
        <h2>Shortlist de <?php echo $mesAtual; ?></h2>
        <?php foreach ($shortlist as $key => $value): ?>
          <button class="shortlist-item item-<?php echo $key; ?> <?php if($key == 0){echo 'active';} ?>" data-target="item-<?php echo $key; ?>"><?php echo $shortlist[$key]->exibicao; ?></button>
        <?php endforeach; ?>
    </div>
    <?php foreach ($shortlist as $key => $value): ?>
    <article class="item-<?php echo $key; ?> <?php echo ($key !== 0) ? 'hidden' : ''; ?>">
        <h2 class="item-title sh-title"><?php echo $shortlist[$key]->titulo; ?></h2>
        <p>
          <?php echo $shortlist[$key]->ideia; ?>
        </p>
    </article>
    <?php endforeach; ?>
    <div class="clearfix"></div>
</section>
<?php } ?>
