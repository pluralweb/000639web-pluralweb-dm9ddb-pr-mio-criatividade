<?php if($logged && !is_null($logged)){ ?>
<section class="logged">

  <?php if($month['valid']){ ?>
    <nav>
        <a data-target="cadastre-ideia" href="javascript:void(0);" title="Inscreva sua ideia" class="nav-item cadastre-ideia active">Inscreva sua ideia</a>
        <label class="toggle"><input type="checkbox" class="slider" /><span></span></label>
        <a data-target="regulamento" href="javascript:void(0);" title="Regulamento" class="nav-item regulamento">Regulamento</a>
    </nav>
    <div class="cadastre-ideia content">
      <div class="hideForm">
        <h2>Preencha os campos abaixo:</h2>
        <form id="campos" name="inscricao" method="post">
          <input type="hidden" name="id" value="<?php echo $id; ?>"/>
          <div class="full">
            <label>Título da sua ideia?
              <input type="text" name="titulo"  placeholder="Digite o titulo da sua ideia"/>
            </label>
          </div>
          <div>
            <label>Qual a ideia?
              <textarea name="ideia" placeholder="Digite seu texto aqui"></textarea>
            </label>
          </div>

          <div>
            <label>Qual a sua contribuição pessoal no projeto?
              <textarea name="contr" placeholder="Digite seu texto aqui"></textarea>
            </label>
          </div>

          <div>
            <label>Quem pode atestar sua participação?
              <textarea name="part" placeholder="Digite seu texto aqui"></textarea>
            </label>
          </div>

          <div>
            <label>Por que merece ganhar?
              <textarea name="motivo" placeholder="Digite seu texto aqui"></textarea>
            </label>
          </div>

          <input type="submit" value="> Enviar" />

          <span class="errormsg">Verifique suas informações</span>
        </form>
      </div>
      <div class="showSuccess">
        Sua ideia foi enviado com Sucesso.
        <a href="javascript:void(0);" class="voltarForm">> Enviar outra ideia</a>
      </div>
    </div>
    <?php } ?>
    <!-- .cadastre-ideia -->
    <div class="regulamento content">
        <h2>Regulamento</h2>
        <div class="regulamento-bg">
            <div class="scroll-pane">
                <div class="center">
                    <p><strong>REGULAMENTO</strong></p>
                    <p class="italic"><strong>"Heróis da Criatividade"</strong></p>
                </div>
                <div class="center">
                    <p><strong>DDB BRASIL PUBLICIDADE LTDA. (Realizadora)</strong></p>
                    <p>Avenida Brigadeiro Luís Antônio, 5013</p>
                    <p>CEP: 01401-002 – Jardim Paulista – São Paulo – SP</p>
                    <p>CNPJ nº 60.741.303/0001-97</p>
                    <p>Nome Fantasia: DM9DDB</p>
                </div>
                <p>A presente Ação é instituída na modalidade de “Endomarketing/Incentivo” pela realizadora DDB BRASIL PUBLICIDADE LTDA., com sede na Avenida Brigadeiro Luís Antônio, 5013, município de São Paulo, Brasil, CEP 01401-002, inscrita no CNPJ/MF sob o n° 60.741.303/0001-97, cujo nome fantasia é DM9DDB;</p>
                <p>A Ação, denominada como “Heróis da Criatividade”, consiste no pagamento de prêmios mensais de R$ 5.000,00 (cinco mil reais) e 2 (dois) prêmios anuais no valor de R$ 50.000,00 (cinquenta mil reais), mediante concurso interno, a fim de incentivar os colaboradores da DM9DDB à descoberta de soluções e ações inovadoras com uso da criatividade e que tenham potencial de sucesso para a Realizadora.</p>
                <p>A Ação tem característica exclusivamente motivacional, voluntária e gratuita, visando o engajamento dos colaboradores da DM9DDB, dispensados de qualquer tipo de pagamento pela inscrição.</p>
                <ol>
                    <li>
                        <p class="listtitle">DO PERÍODO DE VIGÊNCIA E DOS PARTICIPANTES</p>
                        <ol>
                            <li>
                                <p>A promoção terá início no dia <strong>4 de maio de 2016</strong> e término em <strong> 30 de dezembro de 2016</strong>, podendo ser prorrogada, cancelada ou alterada a qualquer momento pela Realizadora, a seu exclusivo critério e sem qualquer ônus.</p>
                            </li>
                            <li>
                                <p>Na hipótese de cancelamento antecipado ou alteração da presente Ação, a Realizadora se compromete a divulgar sua decisão da mesma forma e no mesmo veículo em que foi divulgada a Ação.</p>
                            </li>
                            <li>
                                <p>Sem prejuízo do acima, a DM9DDB poderá, a seu exclusivo critério, a qualquer tempo e em qualquer localidade, utilizar as ideias cadastradas, independentemente de tais ideias terem sido premiadas ou não no âmbito da presente Ação.</p>
                            </li>
                            <li>
                                <p>A Ação é direcionada exclusivamente aos colaboradores da DM9DDB, com exceção dos colaboradores do Board (presidentes e vice-presidentes) que integrarão o Comitê de Avaliação.</p>
                                <ol>
                                    <li>
                                        <p>Terceiros e freelancers não poderão participar.</p>
                                    </li>
                                    <li>
                                        <p>Caso algum dos contemplados se enquadre nesta cláusula acima, ele será automaticamente desclassificado.</p>
                                    </li>
                                    <li>
                                        <p>Os colaboradores que durante a vigência desta Ação forem desligados, pedirem demissão ou estiverem de licença médica (afastados pelo INSS) não poderão participar e não farão jus a nenhuma premiação.</p>
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p class="listtitle">DESCRIÇÃO DA MECÂNICA DE PARTICIPAÇÃO</p>
                        <ol>
                            <li>
                                <p>A presente Ação consiste no pagamento de prêmios mediante concurso interno, por meio do qual:</p>
                                <ul>
                                    <li>
                                        <p>Os colaboradores deverão inscrever mensalmente as suas ideias ou soluções criativas que julguem ter potencial de sucesso para a DM9DDB. O Comitê de Avaliação escolherá todo mês a melhor ideia que será premiada nos termos do item 4.</p>
                                    </li>
                                    <li>
                                        <p>Os Participantes ganhadores dos prêmios mensais, do período de 04 de maio de 2016 até 30 de dezembro de 2016, concorrerão ao prêmio anual. O Comitê de Avaliação escolherá os 2 (dois) Participantes com as ideias mais criativas, sendo um do departamento da Criação e o outro de um dos demais departamentos da DM9DDB, que serão premiados nos termos do item 4. </p>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>O colaborador terá do dia 1º ao 31 para fazer a inscrição da sua ideia criativa. A inscrição deverá ser realizada via link divulgado no e-mail MKT, o qual poderá ser acessado por meio de qualquer dispositivo com acesso à internet. Após a inscrição, o Participante receberá, no prazo de até 5 (cinco) dias úteis, um e-mail confirmando o sucesso ou não da inscrição. O Participante será desclassificado caso não atenda aos requisitos do presente Regulamento, bem como não preencha os itens obrigatórios descritos no momento da inscrição.</p>
                            </li>
                            <li>
                                <p>Para que as ideias ou soluções criativas sejam consideradas de potencial interesse para a DM9DDB e possam concorrer à premiação, deverão necessariamente enquadrar-se em um dos itens abaixo:</p>
                                <ul>
                                    <li>
                                        <p>Ideias para Cannes;</p>
                                    </li>
                                    <li>
                                        <p>Ideais para nossos clientes;</p>
                                    </li>
                                    <li>
                                        <p>Inovação;</p>
                                    </li>
                                    <li>
                                        <p>Mudança de Processo;</p>
                                    </li>
                                    <li>
                                        <p>Melhoria: O participante deverá apresentar soluções de melhorias de qualquer natureza que seja atualmente praticada pela DM9DDB;</p>
                                    </li>
                                </ul>
                                <ol>
                                    <li>
                                        <p>As ideias e/ou soluções criativas sugeridas pelos Participantes para os itens “Mudança de Processo e Melhoria” deverão ser superiores às já utilizadas pela DM9DDB. O Participante deverá demonstrar porque a ideia agrega valor à DM9DDB no momento da inscrição da ideia.</p>
                                    </li>
                                </ol>
                            </li>
                            <li>
                                <p>As ideias e/ou soluções criativas sugeridas pelos Participantes deverão necessariamente ter relação com a Criatividade + Tecnologia = Resultado para o Negócio.</p>
                            </li>
                            <li>
                                <p>Não há número limite de inscrições, podendo, o mesmo Participante, realizar várias inscrições.</p>
                            </li>
                            <li>
                                <p>A mesma ideia poderá ser inscrita no máximo 2 vezes;</p>
                            </li>
                            <li>
                                <p>O sucesso das ideias referentes às soluções criativas sugeridas pelos Participantes deverá ser demonstrado com respostas para as perguntas abaixo:</p>
                                <ul>
                                    <li>
                                        <p>Qual a ideia?</p>
                                    </li>
                                    <li>
                                        <p>Qual sua contribuição pessoal?</p>
                                    </li>
                                    <li>
                                        <p>Quem pode atestar sua participação (duas pessoas)?</p>
                                    </li>
                                    <li>
                                        <p>Por que você merece ganhar?</p>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>Para escolher as ideias ganhadoras, o Comitê de Avaliação dará notas de 0 até 5 para os itens: Criatividade, Uso da Tecnologia, Resultado/Impacto no Negócio e Fácil Implementação.</p>
                            </li>
                            <li>
                                <p>Para os prêmios mensais, o Comitê de Avaliação terá do dia 1º até 05 do mês subsequente à inscrição para votar nas 3 (três) melhores ideias criativas e anunciar, no dia 10 do mês subsequente, a ideia ganhadora. </p>
                            </li>
                            <li>
                                <p>Para os prêmios anuais, o Comitê de Avaliação terá do dia 09 até 13 de janeiro de 2017 para escolher as 2 (duas) melhores ideias criativas e anunciar os Participantes ganhadores no dia 19 de janeiro de 2017.</p>
                            </li>
                            <li>
                                <p>Caso o Comitê de Avaliação julgue que nos prêmios mensais não haja ganhador, por qualquer motivo, os respectivos valores acumulados para o prêmio anual.</p>
                            </li>
                            <li>
                                <p>Caso o Comitê de Avaliação julgue que nos prêmios anuais não haja ganhador, por qualquer motivo, os respectivos valores (mesmo que valores acumulados dos prêmios mensais) serão destinados a outras ações que a Realizadora considere necessárias.</p>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p class="listtitle">O COMITÊ DE AVALIAÇÃO</p>
                        <ol>
                            <li>
                                <p>Será criado um Comitê de Avaliação multidisciplinar, compostos pelo Board da DM9DDB (presidentes e vice-presidentes), todos escolhidos a critério exclusivo da Realizadora e na quantidade por ela definida, e que será responsável por toda a condução da Ação, escolha das ideias vencedoras e auditoria para conferência do cadastro e confirmação dos ganhadores.</p>
                            </li>
                            <li>
                                <p>De 04 de maio de 2016 até 30/12/2016 o Comitê de Avaliação escolherá mensalmente a ideia e/ou solução mais criativa, interessante e viável, sendo considerada, portanto, ganhadora do mês.</p>
                            </li>
                            <li>
                                <p>Em janeiro/2017, o Comitê de Avaliação escolherá, dentre os ganhadores do período de 04 de maio de 2016 até 30 de dezembro de 2016, os 2 (dois) Participantes com as ideias mais criativas, sendo um do departamento da Criação e o outro de um dos demais departamentos da DM9DDB.</p>
                            </li>
                            <li>
                                <p>Para escolher as ideias ganhadoras, o Comitê de Avaliação dará notas de 0 até 5 para os itens: Criatividade, Uso da Tecnologia, Resultado/Impacto no Negócio e Fácil Implementação.</p>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p class="listtitle">DOS PRÊMIOS</p>
                        <ol>
                            <li>
                                <p>Mensalmente será escolhido 1 participante com a ideia vencedora, ao final do ano teremos 8 ganhadores, conforme critério definido. Cada participante vencedor será contemplado com um prêmio em dinheiro de R$ 5.000,00 (cinco mil reais), no mês subsequente à ideia vencedora.</p>
                            </li>
                            <li>
                                <p>Em janeiro/2017 serão escolhidos, dentre os ganhadores do período de 25 de abril de 2016 até 30 de dezembro de 2016, os 2 (dois) Participantes com as ideias mais criativas, sendo um do departamento da Criação e o outro de um dos demais departamentos da DM9DDB, que serão premiados com R$ 50.000,00 (cinquenta mil reais) cada um.</p>
                            </li>
                            <li>
                                <p>Caso sejam inscritas ideias e/ou soluções criativas repetidas, cabe ao Comitê de Avaliação decidir se será considerada apenas a que for cadastrada primeiro, sendo desclassificada a segunda inscrição ou se o prêmio será dividido.</p>
                            </li>
                            <li>
                                <p>A premiação não poderá ser transferida a terceiros, seja a que título, época ou pretexto, devendo, portanto, ser usufruída pelo próprio Participante premiado. Por igual, o valor intrínseco ou econômico da premiação acima descrita não integrará a remuneração (direta ou indireta) do Participante, não tem natureza salarial e não será considerado ou caracterizado como remuneração.</p>
                            </li>
                            <li>
                                <p>No ato da premiação, as obrigações e responsabilidade da DM9DDB extinguem-se por completo.</p>
                            </li>
                            <li>
                                <p>A DM9DDB se reserva ao direito de modificar, aumentar, diminuir, trocar ou de qualquer forma alterar a premiação sem prévio aviso, a seu exclusivo critério. Neste caso, comunicará a todos os Participantes por meio do site da Ação e do e-mail.</p>
                            </li>
                            <li>
                                <p>Além da premiação supra, as ideias vencedoras virarão projetos da DM9DDB, e eventualmente poderão ou não ser implantados, a exclusivo critério da Realizadora. </p>
                            </li>
                            <li>
                                <p>Conforme disposto na cláusula 2.11, caso o Comitê de Avaliação julgue que nos prêmios mensais não haja ganhador, por qualquer motivo, os respectivos valores ficarão acumulados para o prêmio anual.</p>
                            </li>
                            <li>
                                <p>Conforme disposto na cláusula 2.12, Caso o Comitê de Avaliação julgue que não houveram ideias ganhadoras nos prêmios anuais, os respectivos valores serão destinados a outras ações que a Realizadora considere necessária.</p>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p class="listtitle">DIVULGAÇÃO DA AÇÃO E DOS RESULTADOS</p>
                        <ol>
                            <li>
                                <p>A Ação será divulgada via e-mail para todos os colaboradores.</p>
                            </li>
                            <li>
                                <p>A divulgação do resultado da Ação será realizada mensalmente por meio de e-mail marketing interno, bem como estará disponível no site da campanha.</p>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p class="listtitle">DA ENTREGA DOS PRÊMIOS</p>
                        <ol>
                            <li>
                                <p>Os Participantes ganhadores receberão o prêmio em até 20 (vinte) dias após a divulgação do resultado.</p>
                            </li>
                            <li>
                                <p>Os Participantes contemplados deverão assinar o Termo de Recebimento do Prêmio, para que o prêmio seja depositado.</p>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p class="listtitle">DA CONFIDENCIALIDADE</p>
                        <ol>
                            <li>
                                <p>Acordam as Partes que os Participantes deverão manter a confidencialidade integral sobre as ideias cadastradas no âmbito da presente Ação pelo prazo de 5 (cinco) anos.</p>
                            </li>
                            <li>
                                <p>Ainda que a ideia do Participante não seja escolhida no âmbito da presente premiação, o prazo supra deverá ser respeitado.</p>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p class="listtitle">CONDIÇÕES GERAIS</p>
                        <ol>
                            <li>
                                <p>Todo o material produzido a partir dos cadastros das ideias, bem como o uso dos direitos de propriedade intelectual poderão ser mantidos pela DM9DDB em formato digital e em qualquer outro meio, para fins institucionais por tempo indeterminado.</p>
                            </li>
                            <li>
                                <p>Serão automaticamente desclassificados os Participantes que agirem de má-fé ou que, de alguma forma, burlarem as regras e condições deste Regulamento ou utilizarem mecanismos fraudulentos ou ilícitos.</p>
                            </li>
                            <li>
                                <p>A presente Ação poderá ser alterada, suspensa e/ou cancelada a qualquer tempo, sem aviso prévio, por motivo de força maior ou por qualquer outro ou motivo que esteja fora do controle da Realizadora;</p>
                            </li>
                            <li>
                                <p>A Realizadora não poderá ser responsabilizada por nenhum dano ou prejuízo oriundo da participação na Ação “Heróis da Criatividade”;</p>
                            </li>
                            <li>
                                <p>Somente a Realizadora poderá decidir sobre eventuais questões não especificadas neste Regulamento, com prudência e razoabilidade, tendo a sua decisão caráter definitivo e irrecorrível;</p>
                            </li>
                            <li>
                                <p>A mera participação nesta Ação implica no conhecimento e na total aceitação do disposto neste Regulamento;</p>
                            </li>
                            <li>
                                <p>O prêmio é pessoal e intransferível;</p>
                            </li>
                            <li>
                                <p>Todos os Participantes reconhecem e concordam que, em hipótese alguma, a premiação da Ação se caracterizará como remuneração, bônus, bonificação, gratificação ou acréscimo salarial de qualquer natureza;</p>
                            </li>
                            <li>
                                <p>As ideias não podem ser divulgadas em outras redes sociais, somente no site da Ação;</p>
                            </li>
                            <li>
                                <p>As ideias vencedoras com foco em nossos clientes serão apresentadas ao respectivo cliente sem qualquer compromisso de implantação;</p>
                            </li>
                            <li>
                                <p>Caso o Participante perceba ato de má-fé por algum outro participante, deverá enviar um e-mail para a equipe de Compliance.</p>
                            </li>
                            <li>
                                <p>O Participante declara, sob responsabilidade civil e criminal, ser o responsável pela sua participação nas mecânicas envolvidas na Ação, declarando que não está violando direitos de terceiro, bem como assumindo toda a responsabilidade por eventuais reivindicações e reclamações de terceiros, ciente de que não terá direito a qualquer premiação decorrente de atitudes que contrariem os termos deste Regulamento;</p>
                            </li>
                            <li>
                                <p>Este Regulamento é regido pelas leis da República Federativa do Brasil. Fica eleito o foro da comarca de São Paulo, Estado de São Paulo, para conhecer e julgar quaisquer questões decorrentes do Regulamento, com exclusão de qualquer outro, por mais privilegiado que seja.</p>
                            </li>
                        </ol>
                    </li>
                </ol>
            </div>
        </div>
        <!-- .regulamento-bg -->
    </div>

</section>

<section class="ideias">
    <h2>Conheça as ideias vencedoras:</h2>
    <ul>
        <?php foreach ($finalista as $fin) { ?>
          <li class="<?php echo (isset($fin[1]->id)) ? 'unblocked' : 'blocked' ; ?>">
              <a href="javascript:void(0);" <?php echo (isset($fin[1]->id)) ? 'data-mes="'.$fin[0].'"' : '' ; ?>>
                  <?php if(isset($fin[1]->id)){ ?>
                    <img src="<?php echo base_url($fin[1]->img); ?>" />
                  <?php } ?>
                  <div style="color: <?php echo (isset($fin[1]->cor)) ? $fin[1]->cor : '#fff'; ?>">

                      <?php
                      if(isset($fin[1]->exibicao)){
                        echo $fin[1]->exibicao.'<br />';
                      }
                      ?>
                      <span class="month"><?php echo $fin[0]; ?></span></div>
              </a>
          </li>
        <?php } ?>
    </ul>
    <div class="clearfix"></div>
</section>
<div class="replaceData">
  <?php if(!is_null($destaque)){ ?>
    <section class="ideiadomes">
      <div class="bg-responsive"></div>
      <div class="ideiadomes_img">
        <div>
          <span class="light light-1"></span>
          <span class="light light-2"></span>
          <span class="light light-3"></span>
          <span class="light light-4"></span>
          <span class="light light-5"></span>
          <span class="light light-6"></span>
          <span class="light light-7"></span>
        </div>
        <img class="imgReplace" src="<?php echo base_url($destaque[0]->imgLamp); ?>" alt="Ideia do Mês" />
        <div class="subtitle">
          <?php if($destaque[0]->exibicao){
            echo '<span>'. $destaque[0]->exibicao .'</span>';
          } ?>
          <?php if($destaque[0]->cargo){
            echo '<span class="small">'. $destaque[0]->cargo .'</span>';
          } ?>
        </div>
      </div>
      <article>
        <h3>Ideia de <?php echo $destaque[0]->mes; ?></h3>
        <h2><?php echo $destaque[0]->titulo; ?></h2>
        <p><?php echo $destaque[0]->ideia; ?></p>
      </article>
      <div class="clearfix"></div>
    </section>
    <?php } ?>
    <?php if($shortlist){ ?>
      <section class="shortlist">
        <div class="shortlist-titles">
          <h2>Shortlist de <?php echo $mesAtual; ?></h2>
          <?php foreach ($shortlist as $key => $value): ?>
            <button class="shortlist-item item-<?php echo $key; ?> <?php if($key == 0){echo 'active';} ?>" data-target="item-<?php echo $key; ?>"><?php echo $shortlist[$key]->exibicao; ?></button>
          <?php endforeach; ?>
        </div>
        <?php foreach ($shortlist as $key => $value): ?>
          <article class="item-<?php echo $key; ?> <?php echo ($key !== 0) ? 'hidden' : ''; ?>">
            <h2 class="item-title sh-title"><?php echo $shortlist[$key]->titulo; ?></h2>
            <p>
              <?php echo $shortlist[$key]->ideia; ?>
            </p>
          </article>
        <?php endforeach; ?>
        <div class="clearfix"></div>
      </section>
      <?php } ?>
</div>
<?php } ?>
