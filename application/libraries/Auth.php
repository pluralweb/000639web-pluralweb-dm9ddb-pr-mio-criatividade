<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth
{
  	protected 	$ci;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->library('session');
	}

	public function is_logged(){
		$user_id = $this->ci->session->userdata('id');

		if($user_id) {
			$user_exists = $this->ci->account_model->exists_by_id($user_id);

			if(!$user_exists) {
				$this->unset_cookie();
				$this->ci->session->sess_destroy();
			}
			else {
				return $user_id;
			}
		}

		return false;
	}

	public function set_logged($id){
		$this->ci->session->set_userdata(array('logged' => true, 'id' => $id));
		$this->set_cookie($id);
	}

	public function set_cookie($id){
		$this->ci->input->set_cookie('logged', base64_encode($id), 60*60*24*30);
	}

	public function unset_cookie() {
		$this->ci->input->set_cookie(array('name' => 'logged', 'expire' => -1));
	}
}

/* End of file Auth.php */
/* Location: ./application/libraries/Auth.php */
