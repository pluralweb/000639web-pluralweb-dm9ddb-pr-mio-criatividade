<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct(){
		 parent::__construct();
	}

	public function index(){
		if(!$this->session->userdata('email')){
			$this->session->unset_userdata('logged');
		}else {
			redirect('/criatividade');
		}

		$data['logged'] = $this->session->userdata('logged');
		$dataF['logged'] = $data['logged'];

		$isValid = $this->configuracao_model->isValid();

		if($isValid['valid']){
            $mes = explode('-' , $isValid['begin']);
			$isValid['begin'] = $this->month($mes[1]);

			$isValid['end'] = date('d/m' , strtotime($isValid['end']));
		}

		$dataF['month'] = $isValid;

		$this->load->view('site/header', $data);

		$this->load->view('site/footer', $dataF);

	}

	public function logged(){
		if(!$this->session->userdata('email')){
			session_destroy();
		}

		$data['logged'] = $this->session->userdata('logged');

		$data['full'] = false;

		$meses = $this->configuracao_model->getMeses();
		$temp = array();
		$objDefault = new stdClass();

		for ($i=$meses[0]->inicio; $i <= $meses[0]->fim; $i++) {
			array_push($temp, array($this->month($i), $objDefault));
		}

		$mes  = date('m');

		$finalistaLista = $this->cadastros_model->getFinalistasListagem();
		$finalistaMes = $this->cadastros_model->getFinalistas($mes);
		$isValid = $this->configuracao_model->isValid();
		$data['shortlist'] = $this->cadastros_model->getShortList($mes);

		$data['mesAtual'] = $this->month($mes);

		if($isValid['valid']){
            $mes = explode('-' , $isValid['begin']);
            $isValid['begin'] = $this->month($mes[1]);

			$isValid['end'] = date('d/m' , strtotime($isValid['end']));
		}

		if($finalistaMes){
			$data['full'] = true;
			$finalistaMes[0]->mes = $this->month($finalistaMes[0]->mes);
			$data['destaque'] = $finalistaMes;
		}

		if($finalistaLista){
			foreach ($finalistaLista as $key => $value) {
				foreach ($temp as $key2 => $value2) {
					if($temp[$key2][0] == $this->month($finalistaLista[$key]->mes)){
						$temp[$key2][1] = $finalistaLista[$key];
					}
				}
			}
		}

		$data['finalista'] = $temp;

		$dataH['email'] = $this->session->userdata('email');
		$dataH['id'] = $this->session->userdata('id');

		$dataH['logged'] = $this->session->userdata('logged');

		$data['month'] = $isValid;

		$this->load->view('site/header', $dataH);
		$this->load->view('site/content', $data);
		$this->load->view('site/footer', $data);
	}

	public function getInfo(){

		$mes = $this->input->post('mes');

		$data = array();

		$data['mesAtual'] = $this->month($mes);

		$finalistaMes = $this->cadastros_model->getFinalistas($mes);
		$data['shortlist'] = $this->cadastros_model->getShortList($mes);

		if($finalistaMes){
			$finalistaMes[0]->mes = $this->month($finalistaMes[0]->mes);
			$data['destaque'] = $finalistaMes;
		}

		$view = $this->load->view('site/shortlist/shortlist', $data, true);

		echo json_encode($view);

	}

	public function logout(){

		$this->session->unset_userdata('logged');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('nome');

		$this->input->set_cookie('logged', '');

		session_destroy();

		redirect('/');
	}

	public function login()
	{
		$email = trim($this->input->post('email'));
		$data = array(
			'erro' => 1,
			'msg' => 'Há um problema com seu login'
		);

		$returnLogin = $this->user_model->getMentorEmail($email);

		if($returnLogin){
				$this
				->session
				->set_userdata(
												array(
													'logged' => true,
													'id' => $returnLogin->id,
													'email' => $returnLogin->email,
												)
											);

				$this->usuarios_model->edit_user($returnLogin->id, array('status' => 1));

				$data['erro'] = 0;
				$data['msg'] = 'Sucesso';
		}

		echo json_encode($data);
	}

	public function saveCampos()
	{
		$erro = array(
			'erro' => 1,
			'msg' => 'Houve um problema com seu cadastro. Tente mais tarde.'
		);

		$data  = array(
									'idUser' => $this->input->post('id'),
									'status' => 0,
									'titulo' => $this->input->post('titulo'),
									'ideia' => $this->input->post('ideia'),
									'contribuicao' => $this->input->post('contr'),
									'quempode' => $this->input->post('part'),
									'merece' => $this->input->post('motivo'),
									'data_add' => date('Y-m-d H:i:S')
									);

		$saveData = $this->cadastros_model->insert_cadastro(CAMPOS_TABLE, $data);

		if($saveData){
				$erro['erro'] = 0;
				$erro['msg'] = 'Sucesso';
		}

		echo json_encode($erro);

	}
	private function month($mes){
		switch ($mes){
			case 1: $mes = "Janeiro"; break;
			case 2: $mes = "Fevereiro"; break;
			case 3: $mes = "Março"; break;
			case 4: $mes = "Abril"; break;
			case 5: $mes = "Maio"; break;
			case 6: $mes = "Junho"; break;
			case 7: $mes = "Julho"; break;
			case 8: $mes = "Agosto"; break;
			case 9: $mes = "Setembro"; break;
			case 10: $mes = "Outubro"; break;
			case 11: $mes = "Novembro"; break;
			case 12: $mes = "Dezembro"; break;
		}
		return $mes;
	}
	}
