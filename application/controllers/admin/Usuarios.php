<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'usuarios';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');
 		if(!in_array(6,$dataH['permissoes'])) redirect('admin/home/');
    $data['usuarios'] = $this->usuarios_model->get_usuarios();
		$i=0;
		foreach($data['usuarios'] as $user) {
			$data['usuarios'][$i]->permissoes = $this->usuarios_model->get_permissoes_user($user->id);
			$i++;
		}

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/usuarios_listagem', $data);
	}

	public function inserir()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'usuarios';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');
 		if(!in_array(6,$dataH['permissoes'])) redirect('admin/home/');

		$data['permissoes'] = $this->usuarios_model->get_permissoes();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/usuarios_inserir', $data);
	}

	public function addUsuario() {

		$data = array(
			'nome' => $this->input->post('nome'),
			'usuario' => $this->input->post('usuario'),
			'senha' => md5($this->input->post('senha')),
			'data_add' => date('Y-m-d H:i:s'),
			'status' => '1'
		);

		$idCadastrado = $this->usuarios_model->insert_usuario($data);

		$permissoes = $this->input->post('permissoes');

		foreach($permissoes as $perm) {
			$dataPerm = array(
				'id_usuario' => $idCadastrado,
				'id_permissao' => $perm
			);
			$this->usuarios_model->insert_permissao($dataPerm);
		}

		redirect( 'admin/usuarios' );
	}


	public function editar()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$id = $this->uri->segment(4);

		$dataH['sessao'] = 'usuarios';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');
 		if(!in_array(6,$dataH['permissoes'])) redirect('admin/home/');

		$data['usuario'] = $this->usuarios_model->getUsuario($id);
		$data['permissoesUser'] = $this->usuarios_model->get_permissoes_user($id);
		$data['permissoes'] = $this->usuarios_model->get_permissoes();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/usuarios_editar', $data);
	}

	public function edtUsuario() {

		$id = $this->input->post('id');
		$senha = $this->input->post('senha');
		$data = array(
			'nome' => $this->input->post('nome'),
			'usuario' => $this->input->post('usuario'),
			'data_edt' => date('Y-m-d H:i:s')
		);
		if(!empty($senha)){
			$data['senha'] = md5($this->input->post('senha'));
		}


		$idCadastrado = $this->usuarios_model->edit_user($id, $data);

		$this->usuarios_model->delete_permissoes($id);
		$permissoes = $this->input->post('permissoes');

		foreach($permissoes as $perm) {
			$dataPerm = array(
				'id_usuario' => $idCadastrado,
				'id_permissao' => $perm
			);
			$this->usuarios_model->insert_permissao($dataPerm);
		}

		redirect( 'admin/usuarios' );
	}

	public function excluir() {

		$id = $this->uri->segment(4);

		$data = array(
			'status' => 0
		);

		$idCadastrado = $this->usuarios_model->edit_user($id, $data);
		redirect( 'admin/usuarios' );
	}


}
