<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'mentores';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');
		$dataH['tipo'] = (strrpos($this->session->userdata('user'), '@dm9') > 0) ? true : false;
		if(!in_array(6,$dataH['permissoes'])) redirect('admin/home/');

		$data['mentores'] = $this->user_model->getAll();

		array_merge($data, $dataH);

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/user_listagem', $data);
	}

		public function inserir()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'mentores';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');
		$dataH['tipo'] = (strrpos($this->session->userdata('user'), '@dm9') > 0) ? true : false;
		if(!in_array(6,$dataH['permissoes'])) redirect('admin/home/');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/user_inserir');
	}

	public function add() {

		$data = array(
			'nome' => trim($this->input->post('nome')),
			'email' => trim($this->input->post('email'))
		);

		$id = $this->user_model->insert($data);

		redirect('admin/user/listagem');

	}

	public function addResposta() {
		$data = array(
			'id_profile' => $this->input->post('id'),
			'id_mentor' => $this->input->post('id_mentor'),
			'mensagem' => $this->input->post('bio')
		);

		$aprovar = ($this->input->post('aprovar') == 1) ? 1 : 0;

		//echo 'aprovar ' . $aprovar;

		if ($aprovar == 1) {
			$data['mensagem'] = NULL;
		}

		$idMentorando = $this->profiles_model->getMentorando($data['id_profile']);

		$id = $this->profiles_model->insertPorque($data);

		$data2 = array('moderado' => $aprovar, 'closed'=> 1);
		if ($aprovar == 0) {
			$data2['idMentor'] = NULL;
		}

		$id = $this->profiles_model->edit($data['id_profile'], $data2);

		if ($id) {
			$r = $this->sendEmail($aprovar, $idMentorando->email, $data['id_profile'], $data['id_mentor']);
		}

		redirect('admin/profiles/listagem');

	}

	public function editar(){
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$id = $this->uri->segment(4);

		$dataH['sessao'] = 'mentores';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');

		if(!in_array(6,$dataH['permissoes'])) redirect('admin/home/');

		$data['mentor'] = $this->user_model->getMentor($id);
		$data['id'] = $id;

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/user_editar', $data);
	}

	public function edt() {
		$id = $this->input->post('id');

		$data = array(
			'nome' => $this->input->post('nome'),
			'email' => $this->input->post('email')
		);

		$this->user_model->edit($id, $data);

		redirect('admin/user/listagem');
	}

	public function excluir() {

		$id = $this->uri->segment(4);

		$data = array(
			'status' => 1
		);

		$idCadastrado = $this->user_model->edit($id, $data);
		redirect( 'admin/user/listagem' );
	}

	public function exportar() {
		$this->load->helper('download');
		$this->load->library('PHPReport');

		$data = date('d-m-Y');

		$cadastros = $this->user_model->getAllArrayUser();

		$template = 'mentor.xlsx';
					//set absolute path to directory with template files
		$templateDir = APPPATH . "views/template/";

		$dadosExport = array();
		$cont = 0;

		foreach ($cadastros as $c) {
			$id = $c['id'];
			$musica = $this->user_model->getAllInteressesByIdTipo($id, 1);
			$filme = $this->user_model->getAllInteressesByIdTipo($id, 2);
			$livros = $this->user_model->getAllInteressesByIdTipo($id, 3);

			$c['senha'] = '123456';

			$temp = array();
			foreach ($filme as $m) {
				array_push($temp, $m->interesse);
			}
			$filme = $temp;
			$temp = array();

			foreach ($livros as $m) {
				array_push($temp, $m->interesse);
			}
			$livros = $temp;
			$temp = array();

			foreach ($musica as $m) {
				array_push($temp, $m->interesse);
			}
			$musica = $temp;

			$dadosExport[$cont] = $c;
			$dadosExport[$cont]['musicas'] = implode(', ', $musica);
			$dadosExport[$cont]['filmes'] = implode(', ', $filme);
			$dadosExport[$cont]['livros'] = implode(', ', $livros);

			$dadosExport[$cont]['imgModal'] = base_url(IMAGENS_FOLDER.$c['imgModal']);
			$dadosExport[$cont]['imgGrande'] = base_url(IMAGENS_FOLDER.$c['imgGrande']);
			$dadosExport[$cont]['thumb'] = base_url(IMAGENS_FOLDER.$c['thumb']);

			$cont++;
		}

		//set config for report
		$config = array(
			'template' => $template,
			'templateDir' => $templateDir
		);


		//load template
		$R = new PHPReport($config);

		$R->load(array(
						'id' => 'cadastros',
						'repeat' => TRUE,
						'data' => $dadosExport
				)
		);
		// define output directoy
		$output_file_dir = "";


		$output_file_excel = $output_file_dir  . 'mentores-' . $data;
		//download excel sheet with data in /tmp folder
		$result = $R->render('excel', $output_file_excel);
	}

	private function do_upload($imagem, $folder, $w=false, $h=false){
		$this->load->library('upload');

		$config['upload_path'] 		= './' . $folder;
		$config['allowed_types'] 	= 'jpg|png';
		$config['max_size']			= '5120';
		$config['encrypt_name'] 	= TRUE;

		$this->upload->initialize($config);


		if ($this->upload->do_upload($imagem)){
			$upload_data = $this->upload->data();

			if($w && $h) {
				$image_config["image_library"] = "gd2";
				$image_config["source_image"] = './'.$folder.'/'.$upload_data['file_name'];
				$image_config['create_thumb'] = FALSE;
				$image_config['maintain_ratio'] = TRUE;
				$image_config['new_image'] = './'.$folder.'/'.$upload_data['file_name'];
				$image_config['quality'] = "100%";
				$image_config['width'] = $w;
				$image_config['height'] = $h;
				$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
				$image_config['master_dim'] = ($dim > 0)? "height" : "width";

				$this->load->library('image_lib');
				$this->image_lib->initialize($image_config);

				if(!$this->image_lib->resize()){ //Resize image
					return false;
				}else{
					$image_config['image_library'] = 'gd2';
					$image_config['source_image'] = './'.$folder.'/'.$upload_data['file_name'];
					$image_config['new_image'] = './'.$folder.'/'.$upload_data['file_name'];
					$image_config['quality'] = "100%";
					$image_config['maintain_ratio'] = FALSE;
					$image_config['width'] = $w;
					$image_config['height'] = $h;
					$image_config['x_axis'] = '0';
					$image_config['y_axis'] = '0';

					$this->image_lib->clear();
					$this->image_lib->initialize($image_config);

					if (!$this->image_lib->crop()){
							return false;
					}else{
						return $upload_data['file_name'];
					}
				}
			} else {
				return $upload_data['file_name'];
			}
		} else {
			return false;
		}
	}
	private function sendEmail($tipo, $emailm, $id, $idMentor){
		$mandrill_ready = NULL;
		$result = 'fail';
		$send = FALSE;
		$dataEmail = array();
		$email = array(
			'html' => '',
			'text' => "DM9DDB Mentoring",
			'subject' => "DM9DDB Mentoring - Status do Mentor",
			'from_email' => 'contato@dm9mentoring.com.br',
			'from_name' => "DM9DDB Mentoring"
		);

		$title = $this->configuracoes_model->getField('titulo');
		$dataEmail['title'] = $title->titulo;
		$email['to'] = array(array('email' => $emailm));

		$dataEmail['porque'] = $this->profiles_model->getPorque($id, $idMentor);

		$email['html'] = $this->load->view('email/reprova', $dataEmail, true);
		if ($tipo == 1) {
			$email['html'] = $this->load->view('email/aprova', $dataEmail, true);
		}
		$send = TRUE;

		if ($send) {
			// Mandrill
			try {
				$this->mandrill->init($this->config->config['mandrill_api_key']);
				$mandrill_ready = TRUE;
			}
			catch(Mandrill_Exception $e) {
				$mandrill_ready = FALSE;
			}

			if($mandrill_ready) {
				//Send us some email!
				$result = $this->mandrill->messages_send($email);
			}
		}
		return $result;

	}
}
