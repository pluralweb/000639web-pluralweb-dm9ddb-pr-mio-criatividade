<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastros extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

        $cadastros = $this->cadastros_model->get_cadastros();

		$data['cadastros'] = array();
		$i=0;

		foreach($cadastros as $cad) {
			$data['cadastros'][$i] = $cad;
			$data['cadastros'][$i]->filhos = $this->cadastros_model->getFilhos($cad->id);
		}

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/cadastros_listagem', $data);
	}

	public function pendentes()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'pendentes';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');

		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

		$data['datas'] = $this->configuracao_model->getMeses();

        $data['cadastros'] = $this->cadastros_model->get_cadastros('0');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/cadastros_pendentes', $data);
	}

	public function shortlist()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'shortlist';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');

		if(!in_array(7,$dataH['permissoes'])) redirect('admin/home/');

				$data['idCampo'] = $this->uri->segment(4);

        $data['idUser'] = $this->cadastros_model->getUserId($data['idCampo']);

        $data['datas'] = $this->configuracao_model->getMeses();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/shortlist_inserir', $data);
	}

public function shortlistSave()
{
    $id = $this->input->post('idCampo');

    $data = array(
        'status' => 1,
        'id_user_aprovacao' => $this->session->userdata('id'),
        'data_aprovacao' => date('Y-m-d H:i:S')
    );

    $idCadastrado = $this->cadastros_model->edit_cadastroCampo($id, $data);

	$meses;

	if($this->input->post('mes') !== 'a'){
		$meses = $this->input->post('mes');
	}else{
		$meses = date('m');
	}

	$data  = array(
					'idUser' => $this->input->post('idUser'),
					'exibicao' => $this->input->post('exibicao'),
					'img' => '',
					'mes' => $meses,
					'imgLamp' => '',
					'idCampo' => $id,
					'cargo' => '',
					'tipo' => 2,
					'cor' => '',
					'data_add' => date('Y-m-d H:i:S')
					);

	$saveData = $this->cadastros_model->insert_cadastro(GANHADORES_TABLE, $data);

	if($saveData){
		redirect('admin/cadastros/pendentes');
	}
}
public function shortlistEdit()
{
    $id = $this->input->post('idCampo');
    $idUser = $this->input->post('idUser');
    $idGanhador = $this->input->post('idGanhador');
    $exibicao = $this->input->post('exibicao');

		if(!empty($exibicao)){
			$data['exibicao'] = $exibicao;
		}else{
			$data['exibicao'] = NULL;
		}
		$meses;

		if($this->input->post('mes') !== 'a'){
			$meses = $this->input->post('mes');
		}else{
			$meses = date('m');
		}

		$data['mes'] = $meses;

	$saveData = $this->cadastros_model->edit_cadastro_custom(GANHADORES_TABLE, $idGanhador , $data);

	if($saveData){
		redirect('admin/destaque');
	}
}

	public function finalistas()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'destaque';
		$dataH['nome'] = $this->session->userdata('nome');

 		$dataH['permissoes'] = $this->session->userdata('permissoes');

		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

        $data['cadastros'] = $this->cadastros_model->getShortListAll();

				foreach ($data['cadastros'] as $cad) {
					$cad->mes = $this->month($cad->mes);
				}

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/cadastros_aprovados', $data);
	}

	public function reprovados()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'reprovados';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

        $data['cadastros'] = $this->cadastros_model->getReprovados();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/cadastros_reprovados', $data);
	}

	public function detalhes()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

		$date = date('m');

		$id = $this->uri->segment(4);

        $data['cadastro'] = $this->cadastros_model->getShortListByID($id);

				$data['finalista'] = $this->cadastros_model->countFinalista($date);
				$data['destaque'] = $this->cadastros_model->countDestaque($date);

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/cadastros_detalhes', $data);
	}

	public function detalhespendentes()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

		$date = date('m');

		$id = $this->uri->segment(4);

        $data['cadastro'] = $this->cadastros_model->gePendenteId($id);

				$data['finalista'] = $this->cadastros_model->countFinalista($date);
				$data['destaque'] = $this->cadastros_model->countDestaque($date);

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/cadastros_detalhes2', $data);
	}

	public function aprovar()
	{
		$ids = $this->input->post('aprovacao');
		$mes = $this->input->post('mes');
		$keys = array_keys($ids);

		if($mes == 'a'){
			$mes = date('m');
		}

		if($keys) {
			foreach($keys as $id) {

				$ideia = $this->cadastros_model->get($id);

				if($ideia->status == 0) {

					if($ids[$id]=='a') {
						redirect( 'admin/cadastros/shortlist/'.$id);

					}
                    else if($ids[$id]=='f') {
						redirect('admin/inserir/'.$id);
					}
                    else if($ids[$id]=='r') {
						$data = array(
							'status' => 2,
							'id_user_aprovacao' => $this->session->userdata('id'),
							'data_aprovacao' => date('Y-m-d H:i:S')
						);
						$idCadastrado = $this->cadastros_model->edit_cadastroCampo($id, $data);
						redirect( 'admin/cadastros/pendentes');
					}
				}

			}
		}
	}

	public function excluir() {

		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$id = $this->uri->segment(4);

		$data = array(
			'status' => 0
		);

		$idCadastrado = $this->cadastros_model->edit_cadastro($id, $data);
		redirect( 'admin/cadastros' );
	}

	public function exportar() {
        $data = date('d-m-Y');
        $this->load->library('Excel');
        $cadastros = $this->cadastros_model->getAprovadosAll();
        $this->excel->to_excel($cadastros, 'cadastros-' . $data);
    }

	public function exportarReprovados() {
        $data = date('d-m-Y');
        $this->load->library('Excel');
        $cadastros = $this->cadastros_model->getReprovadosAll();
        $this->excel->to_excel($cadastros, 'cadastros-reprovados-' . $data);
    }
	public function exportarFinalista() {
        $data = date('d-m-Y');
        $this->load->library('Excel');
        $cadastros = $this->cadastros_model->getReprovadosAll();
        $this->excel->to_excel($cadastros, 'cadastros-reprovados-' . $data);
    }

	public function exportarShortlist() {
        $data = date('d-m-Y');
        $this->load->library('Excel');
        $cadastros = $this->cadastros_model->getFinalistasAll();
        $this->excel->to_excel($cadastros, 'cadastros-reprovados-' . $data);
    }

    public function exportAll()
    {
        if(!$this->session->userdata('id')) redirect('admin/login');
        if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

        $dataH['sessao'] = 'exportar';
        $dataH['subsessao'] = 'todos';
        $dataH['nome'] = $this->session->userdata('nome');
        $dataH['permissoes'] = $this->session->userdata('permissoes');

        if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

        $this->load->view('admin/header', $dataH);
        $this->load->view('admin/exportar');
    }

	public function exportarTodos() {

        $begin = implode('-',array_reverse(explode('/', $this->input->post('inicio'))));

        $fim = implode('-',array_reverse(explode('/', $this->input->post('fim'))));

        $this->load->library('Excel');
        $cadastros = $this->cadastros_model->get_cadastrosAllRange($begin, $fim);

		$dadosExport = array();
		$cont = 0;
		foreach($cadastros as $cad) {
			$dadosExport[$cont]= $cad;
			switch($cad->status) {
				case 0:
					$dadosExport[$cont]->status = 'Pendente';
				break;
				case 1:
					$dadosExport[$cont]->status = 'Shortlist';
				break;
				case 2:
					$dadosExport[$cont]->status = 'Reprovado';
				break;
				case 3:
					$dadosExport[$cont]->status = 'Finalista';
				break;
			}
			$cont++;
		}
        $this->excel->to_excel($dadosExport, 'todos-cadastros-' . $begin . '-'. $fim);
    }

		private function month($mes){
			switch ($mes){
				case 1: $mes = "Janeiro"; break;
				case 2: $mes = "Fevereiro"; break;
				case 3: $mes = "Março"; break;
				case 4: $mes = "Abril"; break;
				case 5: $mes = "Maio"; break;
				case 6: $mes = "Junho"; break;
				case 7: $mes = "Julho"; break;
				case 8: $mes = "Agosto"; break;
				case 9: $mes = "Setembro"; break;
				case 10: $mes = "Outubro"; break;
				case 11: $mes = "Novembro"; break;
				case 12: $mes = "Dezembro"; break;
			}
			return $mes;
		}
	}
