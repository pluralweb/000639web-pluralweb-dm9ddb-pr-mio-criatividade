<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){$this->logout();redirect('admin/login');}

		$dataH['sessao'] = 'home';
		$dataH['subsessao'] = '';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');

		$data = array();
		$data['cadastros'] = $this->usuarios_model->count_total();
		$data['aprovados'] = $this->usuarios_model->count_aprovados();
		$data['reprovados'] = $this->usuarios_model->count_reprovados();
		$data['pendentes'] = $this->usuarios_model->count_pendentes();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/home', $data);
	}

	public function login()
	{
		if(!is_null($this->session->userdata('permissoes'))) redirect('admin/home');
		if($this->session->userdata('id')) redirect('admin/home');
		$this->load->view('admin/login');
	}

	public function invalid(){

		$data['invalid'] = TRUE;

		$this->load->view( 'admin/login', $data );
	}

	public function dologin(){

		if ($this->input->post()){
			$user = $this->input->post('login');
			$pass = $this->input->post('senha');

			$userinfo = $this->usuarios_model->loginAdmin($user, $pass);

			if (!$userinfo)
				redirect( 'admin/home/invalid' );

			$permissoes = $this->usuarios_model->get_permissoes_user($userinfo[0]->id);

			$arrayPerm = array();

			foreach($permissoes as $perm) {
				array_push($arrayPerm, $perm->id);
			}

			$this->session->set_userdata(array('logged' => true, 'id' => $userinfo[0]->id, 'nome' => $userinfo[0]->nome, 'permissoes' => $arrayPerm, 'finalista' => false));
			redirect( 'admin/home' );
		}
	}

	public function logout(){

		$this->session->unset_userdata('logged');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('nome');
		$this->input->set_cookie('logged', '');
		session_destroy();

		redirect('admin/login');
	}

	public function perfil()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){$this->logout();redirect('admin/login');}

		$dataH['sessao'] = 'perfil';
		$dataH['subsessao'] = '';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');

		$data['usuario'] = $this->usuarios_model->getUsuario($this->session->userdata('id'));
		$data['sucesso'] = $this->input->get('editado');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/meu_perfil', $data);
	}

	public function edtUsuario() {

		$idUser = $this->session->userdata('id');

		$dataUsuario = array('nome' =>	$this->input->post('nome'),
							'usuario' => $this->input->post('usuario'),
							'data_edt' => date('Y-m-d H:i:s')
							);
		if($this->input->post('senha')!='') {
			$dataUsuario['senha'] = md5($this->input->post('senha'));
		}
		$this->usuarios_model->edit_user($idUser, $dataUsuario);

		redirect( 'admin/home/perfil/?editado=1' );

	}

	public function error404() {
		$this->load->view('404');
	}


}
