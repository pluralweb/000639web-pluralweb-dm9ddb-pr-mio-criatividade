<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracao extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'configuracao';
		$dataH['subsessao'] = 'inscricao';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(7,$dataH['permissoes'])) redirect('admin/home/');

        $data['cadastros'] = $this->configuracao_model->getDatas();

				if($data['cadastros']){
					foreach ($data['cadastros'] as $dat) {
						$dat->inicio = date("d/m/Y", strtotime($dat->inicio));
						$dat->fim = date("d/m/Y", strtotime($dat->fim));
					};
				}


		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/configuracao_listagem', $data);
	}

	public function meses()	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'configuracao';
		$dataH['subsessao'] = 'meses';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');


		$data['cadastros'] = $this->configuracao_model->getMeses();

		if($data['cadastros']){
			$data['cadastros']['0']->inicio = $this->month($data['cadastros']['0']->inicio);
			$data['cadastros']['0']->fim = $this->month($data['cadastros']['0']->fim);
		}


		$data['hideForm'] = true;

		if(count($data['cadastros']) == 0){
			$datas = array();
			for ($i=1; $i < 13; $i++) {
				$mesExt = $this->month($i);
				$datas[$i] = $mesExt;
			}

			$data['datas'] = $datas;
		}else{
			$data['hideForm'] = false;
		}


		if(!in_array(7,$dataH['permissoes'])) redirect('admin/home/');


		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/configuracao_meses', $data);
	}

	public function deleteMes(){
		$ipt = $this->input->post('delete');

		$id = $this->configuracao_model->deleteMes($ipt);

		redirect('admin/configuracao/meses');

	}

	public function deleteData(){
		$ipt = $this->input->post('delete');

		$id = $this->configuracao_model->deleteData($ipt);

		redirect('admin/configuracao/inscricao');

	}

	public function savedata(){
		$inicio = implode('-', array_reverse(explode('/', $this->input->post('inicio'))));
		$fim = implode('-', array_reverse(explode('/', $this->input->post('fim'))));

		$data = array(
									'inicio' => $inicio,
									'fim' => $fim
								);

		$ipt = $this->configuracao_model->insertData($data);

		if($ipt){
			redirect('admin/configuracao/inscricao');
		}

	}

	public function savemes(){
		$inicio = $this->input->post('inicio');
		$fim = $this->input->post('fim');

		$data = array(
									'inicio' => $inicio,
									'fim' => $fim
								);

		$ipt = $this->configuracao_model->insertMes($data);

		echo $ipt;
	}
	private function month($mes){
		switch ($mes){
			case 1: $mes = "Janeiro"; break;
			case 2: $mes = "Fevereiro"; break;
			case 3: $mes = "Março"; break;
			case 4: $mes = "Abril"; break;
			case 5: $mes = "Maio"; break;
			case 6: $mes = "Junho"; break;
			case 7: $mes = "Julho"; break;
			case 8: $mes = "Agosto"; break;
			case 9: $mes = "Setembro"; break;
			case 10: $mes = "Outubro"; break;
			case 11: $mes = "Novembro"; break;
			case 12: $mes = "Dezembro"; break;
		}
		return $mes;
	}
}
