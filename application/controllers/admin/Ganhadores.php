<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ganhadores extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'finalista';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

		$data['cadastros'] = $this->cadastros_model->getFinalistasListagem();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/cadastros_finalistas', $data);
	}

	public function saveCampos()
	{
        $idCampo = $this->input->post('idCampo');

        $change = array(
					'status' => '3',
					'id_user_aprovacao' => $this->session->userdata('id'),
					'data_aprovacao' => date('Y-m-d H:i:S')
				);

		$idCadastrado = $this->cadastros_model->edit_cadastroCampo($idCampo, $change);
		if($_FILES['file']['name']) {
			$upload_data = $this->do_upload_image('file');
			if ($upload_data['error'] == FALSE && $upload_data['file'] != FALSE){
				$imgPerfil = $upload_data['filePath'];
			} else {
				exit();
			}
		}

		if($_FILES['fileLamp']['name']) {

			$upload_data = $this->do_upload_image('fileLamp');

			if ($upload_data['error'] == FALSE && $upload_data['file'] != FALSE){
				$imgLamp = $upload_data['filePath'];
			} else {
				exit();
			}
		}

		$meses;

		if($this->input->post('mes') !== 'a'){
			$meses = $this->input->post('mes');
		}else{
			$meses = date('m');
		}

		$inputCargo = $this->input->post('cargo');

		$CARGO = ((empty($inputCargo)) ? "" : $inputCargo);

		$data  = array(
									'idUser' => $this->input->post('idUser'),
									'exibicao' => $this->input->post('exibicao'),
									'img' => $imgPerfil,
									'mes' => $meses,
									'imgLamp' => $imgLamp,
									'idCampo' => $idCampo,
									'cargo' => $CARGO,
									'tipo' => 1,
									'cor' => $this->input->post('cor'),
									'data_add' => date('Y-m-d H:i:S')
									);

		$saveData = $this->cadastros_model->insert_cadastro(GANHADORES_TABLE, $data);

		if($saveData){
			$this->session->set_userdata('finalista', true);

			redirect('admin/cadastros/pendentes');
		}
	}

	public function editarCampos()
	{
		$id = $this->input->post('id');
		$cargo = $this->input->post('cargo');
		$exibicao = $this->input->post('exibicao');
		$cor = $this->input->post('cor');
			$data  = array();

			if($_FILES['file']['name']) {

				$upload_data = $this->do_upload_image('file');

				if ($upload_data['error'] == FALSE && $upload_data['file'] != FALSE){
					$data['img'] = $upload_data['filePath'];
				} else {
					exit();
				}
			}

			if($_FILES['fileLamp']['name']) {

				$upload_data = $this->do_upload_image('fileLamp');

				if ($upload_data['error'] == FALSE && $upload_data['file'] != FALSE){
					$data['imgLamp'] = $upload_data['filePath'];
				} else {
					exit();
				}
			}

		if(!empty($cargo)){
			$data['cargo'] = $this->input->post('cargo');
		}

		if(!empty($exibicao)){
			$data['exibicao'] = $this->input->post('exibicao');
		}

		if($this->input->post('mes') !== 'a'){
			$data['mes'] = (strlen($this->input->post('mes')) == 1) ? '0'.$this->input->post('mes') : $this->input->post('mes');
		}

		if(!empty($cor)){
			$data['cor'] = $this->input->post('cor');
		}

		$saveData = $this->cadastros_model->edit_cadastros_especifico(GANHADORES_TABLE, 'id' , $id , $data);

		redirect('admin/ganhadores/detalhes/'.$this->input->post('id'));
	}

	public function detalhes()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

		$id = $this->uri->segment(4);

				$cadastro = $this->cadastros_model->getFinalista($id);
				$cadastro[0]->mes = $this->month($cadastro[0]->mes);
				$data['cadastro'] = $cadastro;


		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/ganhador_detalhes', $data);
	}

	public function editar()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

		$id = $this->input->post('idUser');

				$data['cadastro'] = $this->cadastros_model->getFinalista($id, null);
				$data['meses'] = $this->cadastros_model->mesesFinalistas();
				$data['datas'] = $this->configuracao_model->getMeses();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/editar_ganhador', $data);
	}

	public function editarShortlist()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'cadastros';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
		$dataH['permissoes'] = $this->session->userdata('permissoes');
		if(!in_array(5,$dataH['permissoes'])) redirect('admin/home/');

		$id = $this->uri->segment(4);
				$data['cadastro'] = $this->cadastros_model->getShortListByID($id);
				$data['datas'] = $this->configuracao_model->getMeses();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/editar_shortlist', $data);
	}

	public function inserir()
	{
		if(!$this->session->userdata('id')) redirect('admin/login');
		if(is_null($this->session->userdata('permissoes'))){redirect('admin/login');}

		$dataH['sessao'] = 'ganhadores';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');

		if(!in_array(7,$dataH['permissoes'])) redirect('admin/home/');

				$data['idCampo'] = $this->uri->segment(3);

        $data['idUser'] = $this->cadastros_model->getUserId($data['idCampo']);

        $data['meses'] = $this->cadastros_model->mesesFinalistas();
        $data['datas'] = $this->configuracao_model->getMeses();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/ganhadores_inserir', $data);
	}

	public function uploadImg() {
		$x = $this->input->post('x');
		$y = $this->input->post('y');
		$w = $this->input->post('w');
		$h = $this->input->post('h');

		if($_FILES['file']['name']) {
			$upload_data = $this->do_upload_image('file');
			if ($upload_data['error'] != FALSE && $upload_data['file'] != FALSE){

				$img['file'] = $upload_data['file_name'];

				$config['image_library'] = 'gd2';
				$config['source_image'] = $upload_data['filePath'];
				$config['maintain_ratio'] = FALSE;
				$config['width']  = $data['w'];
				$config['height'] = $data['h'];
				$config['x_axis'] = $data['x'];
				$config['y_axis'] = $data['y'];

				$this->load->library('image_lib', $config);

				if(!$this->image_lib->crop())
				{
						echo $this->image_lib->display_errors();
				}
				redirect('profile');
				echo json_encode($img);

			} else {
				echo json_encode($upload_data);
				exit();
			}
		}
	}
	//crop it


	private function do_upload_image($arq){

		$this->load->library('upload');
		$this->load->helper('directory');

		$folder = UPLOADS_FOLDER;

		$config['upload_path'] 		= $folder;
		$config['allowed_types'] 	= '*';
		$config['max_size']			= '51200';
		$config['encrypt_name'] 	= TRUE;

		$this->upload->initialize($config);

		if ($this->upload->do_upload($arq)){
			$upload_data = $this->upload->data();
			$fileNam = $upload_data['file_name'];
			$error['file'] = $fileNam;
			$error['filePath'] = $folder.'/'.$fileNam;
			$error['error'] = FALSE;
		} else {
			#erro
			$error['file'] = FALSE;
			$error['error'] = $this->upload->display_errors();
		}
		return $error;
	}
	private function month($mes){
		switch ($mes){
			case 1: $mes = "Janeiro"; break;
			case 2: $mes = "Fevereiro"; break;
			case 3: $mes = "Março"; break;
			case 4: $mes = "Abril"; break;
			case 5: $mes = "Maio"; break;
			case 6: $mes = "Junho"; break;
			case 7: $mes = "Julho"; break;
			case 8: $mes = "Agosto"; break;
			case 9: $mes = "Setembro"; break;
			case 10: $mes = "Outubro"; break;
			case 11: $mes = "Novembro"; break;
			case 12: $mes = "Dezembro"; break;
		}
		return $mes;
	}
}
