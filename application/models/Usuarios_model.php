<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

  public function get_usuarios() {
    return $this->db->query("SELECT *
               FROM ".ADMIN_TABLE."
               WHERE status=1
               ORDER BY data_add DESC")->result();
  }

  public function getUsuario($idUsuario) {
    return $this->db->query("SELECT *
               FROM ".ADMIN_TABLE."
               WHERE id=".$idUsuario)->row();
  }

  public function insert_usuario($data){

		$this->db->insert(ADMIN_TABLE, $data);

		return $this->db->insert_id();
	}

	public function insert_permissao($data){

		$this->db->insert(PERMISSOES_USUARIOS_TABLE, $data);

		return $this->db->insert_id();
	}

	public function edit_user($idUser, $data){
		$this->db->where('id', $idUser);
		$this->db->update(ADMIN_TABLE, $data);

		return true;
	}

	public function delete_permissoes($idUsuario) {

		$this->db->delete(PERMISSOES_USUARIOS_TABLE, array('id_usuario', $idUsuario));

		return true;
	}

   public function get_cadastrosAll($status='1') {
     return $this->db->query("SELECT u.email, u.nome, c.ideia, c.contribuicao, c.quempode, c.merece
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE u.status=".$status."
                              ORDER BY c.data_add ASC")->result();
   }


   public function getAprovados() {
     return $this->db->query("SELECT u.email, u.nome
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE u.status=1
                              ORDER BY c.data_add ASC")->result();
   }

   public function getReprovados() {
     return $this->db->query("SELECT u.email, u.nome
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE u.status=2
                              ORDER BY c.data_add ASC")->result();
   }

   public function get_login($email) {
		$this->db->select("id, email, nome");
    $this->db->like("email", $email);
    return $this->db->get(USER_TABLE)->row();
   }

   public function loginAdmin($user, $password){
		return $this->db->select("id, nome, usuario")
						->where('usuario', $user)
						->where('senha',  md5($password))
						->get(ADMIN_TABLE)
						->result();
	}

   public function get_permissoes_user($id) {
  	   return $this->db->query("SELECT p.*
  								FROM ".PERMISSOES_USUARIOS_TABLE." AS per
  								INNER JOIN ".PERMISSOES_TABLE." AS p
  								ON p.id=per.id_permissao
  								WHERE id_usuario=".$id."")->result();
     }


   public function get_permissoes() {
	   return $this->db->query("SELECT *
								FROM ".PERMISSOES_TABLE."
								ORDER BY nome ASC")->result();
   }


	public function insert_cadastro($data){

		$this->db->insert(CAMPOS_TABLE, $data);

		return $this->db->insert_id();
	}


	public function edit_image($id, $data){
		$this->db->where('id', $id);
		$this->db->update(FILHOS_TABLE, $data);

		return true;
	}

	public function count_cadastros($id) {
		$this->db->where('idUser', $id);
		$this->db->where('status', 1);
		$this->db->from(CAMPOS_TABLE);
		return $this->db->count_all_results();
	}

	public function count_pendentes() {
		$this->db->where('status', 0);
		$this->db->from(CAMPOS_TABLE);
		return $this->db->count_all_results();
	}

	public function count_aprovados() {
		$this->db->where('status', 1);
		$this->db->from(CAMPOS_TABLE);
		return $this->db->count_all_results();
	}

	public function count_reprovados() {
		$this->db->where('status', 2);
		$this->db->from(CAMPOS_TABLE);
		return $this->db->count_all_results();
	}

	public function count_total() {
		$this->db->from(CAMPOS_TABLE);
		return $this->db->count_all_results();
	}

}
