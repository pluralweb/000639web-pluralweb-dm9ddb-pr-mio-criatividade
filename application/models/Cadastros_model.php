<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros_model extends CI_Model {


  public function get_all() {
    return $this->db->query("SELECT u.id, u.email, u.nome, c.id as idCampo, c.ideia, c.data_add
                              FROM ".USER_TABLE." AS u
                              INNER JOIN ".CAMPOS_TABLE." as c
                              ON  c.idUser = u.id
                              ORDER BY c.data_add ASC")->result();
    }

   public function get_cadastros($status=0) {
	   return $this->db->query("SELECT u.id, u.email, u.nome, c.id as idCampo, c.ideia, c.data_add, c.status
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE c.status = ".$status."
                              ORDER BY c.data_add DESC
                              LIMIT 50")->result();
   }

   public function getShortListAll() {
	   return $this->db->query("SELECT u.email, c.id as idCampo, c.ideia, c.data_add, c.status, g.mes, g.exibicao
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              INNER JOIN ".GANHADORES_TABLE." as g
                                ON  g.idCampo = c.id
                              WHERE g.tipo = 2
                              ORDER BY c.data_add ASC
                              LIMIT 50")->result();
   }

   public function getShortListByID($id) {
	   return $this->db->query("SELECT u.id as idUser, u.nome, u.email, c.id as idCampo, c.ideia, c.contribuicao, c.quempode, c.merece, c.data_add, c.status, g.mes, g.exibicao, g.id as idGanhador
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              INNER JOIN ".GANHADORES_TABLE." as g
                                ON  g.idCampo = c.id
                              WHERE g.tipo = 2
                              AND c.id = ".$id)->result();
   }

   public function gePendenteId($id) {
	   return $this->db->query("SELECT u.nome, u.email, c.id as idCampo, c.ideia, c.contribuicao, c.quempode, c.merece, c.data_add, c.status
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE c.id = ".$id)->result();
   }

   public function get_cadastrosAll() {
	   return $this->db->query("SELECT u.nome, u.email, c.ideia, c.contribuicao, c.quempode, c.merece, c.data_add, c.status, MONTH(c.data_aprovacao) as mes
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              ORDER BY c.data_add ASC")->result();
   }

   public function get_cadastrosAllRange($begin, $end) {
	   return $this->db->query("SELECT u.nome, u.email, c.ideia, c.contribuicao, c.quempode, c.merece, c.data_add, c.status, MONTH(c.data_aprovacao) as mes
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE c.data_add BETWEEN '".$begin."' AND '".$end."'
                              ORDER BY c.data_add ASC")->result();
   }

   public function getAprovados() {
	   return $this->db->query("SELECT u.email, u.nome, c.id as idCampo, c.ideia, c.data_add, c.id_user_aprovacao
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE c.status=3
                              ORDER BY c.data_add ASC")->result();
   }
   public function getAprovadosAll() {
	   return $this->db->query("SELECT u.nome, u.email, c.ideia, c.contribuicao, c.quempode, c.merece, c.data_add
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE c.status=1
                              ORDER BY c.data_add ASC")->result();
   }

   public function getReprovados() {
	   return $this->db->query("SELECT u.id, u.email, u.nome, c.id as idCampo, c.ideia, c.data_add
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE c.status=2
                              ORDER BY c.data_add ASC")->result();
   }

   public function getReprovadosAll() {
	   return $this->db->query("SELECT u.nome, u.email, c.ideia, c.contribuicao, c.quempode, c.merece, c.data_add, MONTH(data_aprovacao) as mes
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              WHERE c.status=2
                              ORDER BY c.data_add ASC")->result();
   }

   public function getFinalistas($mes = 4) {
	   return $this->db->query("SELECT g.mes, g.cargo, g.img, g.imgLamp, g.idCampo, g.exibicao, g.tipo, c.ideia, c.titulo
                              FROM ".GANHADORES_TABLE." AS g
                              INNER JOIN ".CAMPOS_TABLE." as c
                                ON c.id = g.idCampo
                              WHERE g.mes = ".$mes."
                              AND g.tipo = 1")->result();
   }
   public function getFinalista($id, $mes = null) {
    $month = "";
     if(!is_null($mes)){
       $month = "AND MONTH(mes) = MONTH(".$mes.")";
     }
	   return $this->db->query("SELECT u.id, u.nome, u.email, c.titulo, c.ideia, c.contribuicao, c.quempode, c.merece, g.id AS idGanhador, g.mes, g.cargo, g.img, g.imgLamp, g.cargo, g.cor, g.idCampo, g.exibicao
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              INNER JOIN ".GANHADORES_TABLE." as g
                                ON  g.idUser = u.id
                              WHERE g.id= ".$id."
                              AND g.tipo = 1
                              ".$month)->result();
   }

   public function getFinalista2($id) {
	   return $this->db->query("SELECT u.id, u.nome, u.email, c.titulo, c.ideia, c.contribuicao, c.quempode, c.merece, g.id AS idGanhador, g.mes, g.cargo, g.img, g.imgLamp, g.cargo, g.cor
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              INNER JOIN ".GANHADORES_TABLE." as g
                                ON  g.idUser = u.id
                              WHERE c.id= ".$id)->result();
   }

   public function getUserId($id) {
	   return $this->db->query("SELECT c.idUser
                              FROM ".CAMPOS_TABLE." AS c
                              WHERE c.id= ".$id)->row();
   }

   public function getUserIds($id) {
	   return $this->db->query("SELECT c.idUser
                              FROM ".CAMPOS_TABLE." AS c
                              WHERE c.id= ".$id)->row();
   }

   public function getFinalistasListagem() {
     return $this->db->query("SELECT u.id, u.nome, u.email, g.id as idGanhador, g.mes, g.img, g.imgLamp, g.idCampo, g.cor, g.exibicao
                              FROM ".GANHADORES_TABLE." AS g
                              INNER JOIN ".USER_TABLE." as u
                               ON  u.id = g.idUser
                              WHERE tipo = 1
                              ORDER BY g.data_add ASC")->result();
   }
   public function getFinalistasAll() {
     return $this->db->query("SELECT u.nome, u.email, g.mes, g.cargo, c.ideia, c.contribuicao, c.quempode, c.merece, c.data_add
                              FROM ".GANHADORES_TABLE." AS g
                              INNER JOIN ".USER_TABLE." as u
                               ON  u.id = g.idUser
                              INNER JOIN ".CAMPOS_TABLE." as c
                               ON  c.idUser = u.id
                              WHERE c.status=3
                              ORDER BY g.data_add ASC")->result();
   }

   public function getShortList($mes = 4) {
	   return $this->db->query("SELECT c.titulo, c.ideia, g.exibicao
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".GANHADORES_TABLE." as g
                                ON  g.idCampo = c.id
                              WHERE g.tipo = 2
                              AND g.mes = ".$mes."
                              ORDER BY c.data_add ASC")->result();
   }

   public function countDestaque($mes = 1) {
	   return $this->db->query("SELECT COUNT(id)
                              FROM ".CAMPOS_TABLE."
                              WHERE status = 1
                              AND MONTH(data_aprovacao) = ".$mes)->result();
   }

   public function countFinalista($mes = 1) {
	   return $this->db->query("SELECT count(g.idUser)
                              FROM ".GANHADORES_TABLE." AS g
                              INNER JOIN ".CAMPOS_TABLE." as c
                                ON  c.idUser = g.idUser
                              WHERE c.status = 3
                              AND g.mes = ".$mes)->result();
   }

   public function get($id, $fin = 0) {
     $campos = "";
     $inner = "";

     if($fin != 0){
       $campos = 'g.exibicao';
       $inner = 'INNER JOIN '.GANHADORES_TABLE.' as g ON g.idUser = c.idUser';
     }

	   return $this->db->query("SELECT u.id, u.email, u.nome, c.id as idCampo, c.ideia, c.contribuicao, c.quempode, c.merece, c.data_add, c.status, c.data_aprovacao ".$campos."
                              FROM ".CAMPOS_TABLE." AS c
                              INNER JOIN ".USER_TABLE." as u
                                ON  u.id = c.idUser
                              ".$inner."
                              WHERE c.id = ".$id)->row();
   }

  public function insert_cadastro($table, $data){

		$this->db->insert($table, $data);

		return $this->db->insert_id();
	}

  public function edit_cadastro_custom($table, $cond, $data){
    $this->db->where('id', $cond);
    $this->db->update($table, $data);
    return $this->db->affected_rows();
	}


	public function edit_cadastro($id, $data){
		$this->db->where('idUser', $id);
		$this->db->update(CAMPOS_TABLE, $data);
		return $this->db->affected_rows();
	}

	public function edit_cadastroCampo($id, $data){
		$this->db->where('id', $id);
		$this->db->update(CAMPOS_TABLE, $data);
		return $this->db->affected_rows();
	}

	public function edit_cadastros($table, $id, $data){
		$this->db->where('idUser', $id);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	public function edit_cadastros_especifico($table, $whichID , $id, $data){
		$this->db->where($whichID , $id);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	public function edit_image($id, $data){
		$this->db->where('id', $id);
		$this->db->update(GANHADORES_TABLE, $data);

		return $this->db->affected_rows();
	}

	public function desativa_cadastro(){
		$data = array('status' => 0);
		$this->db->update(CAMPOS_TABLE, $data);

		return true;
	}

  public function getCadastro($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".CAMPOS_TABLE."
								WHERE id=".$idCadastro)->row();
  }

  public function mesesFinalistas()
  {
    $temp = array();
    $this->db->select('mes');
    $this->db->where('tipo = 1');
		$result = $this->db->get(GANHADORES_TABLE)->result_array();

    foreach ($result as $r) {
      array_push($temp, $r['mes']);
    }

    return $temp;
  }

}
