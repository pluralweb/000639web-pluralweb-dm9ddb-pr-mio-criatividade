<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

  /**
   * Método que retorna as informações
   * de todos os mentores
   * na tabela mentores
   *
   * @return (array->obj) retorna os dados de todos os mentores no banco
   *
   */
  public function getAll($random = FALSE) {
    if($random){
      $this->db->order_by('RAND()');
    }
    $this->db->where('status', 0);
    return $this->db
                ->get(USER_TABLE)->result();
  }

  /**
   * Método que retorna as informações
   * de todos os mentores
   * na tabela mentores na forma de array
   *
   * @return (array->array) retorna os dados de todos os mentores no banco
   *
   */
  public function getAllArray($random = FALSE) {
    $this->db->where('deleted', 0);
    if($random){
      $this->db->order_by('RAND()');
    }
    return $this->db
                ->get(USER_TABLE)->result_array();
  }

  /**
   * Método que retorna as informações
   * de todos os mentores
   * na tabela mentores na forma de array
   *
   * @return (array->array) retorna os dados de todos os mentores no banco
   *
   */
  public function getAllArrayUser() {
    // $this->db->select('m.*, g.*');
    $this->db->where('m.deleted', 0);
    $this->db->join(ADMIN_TABLE.' as g', 'g.nome = m.nome ', 'inner');
        return $this->db
                ->get(USER_TABLE. ' as m')->result_array();
  }

  /**
   * Método que retorna as informações
   * de um mentor aleatorio
   * na tabela mentores
   *
   * @return (obj) retorna os dados de todos os mentores no banco
   *
   */
  public function getRandom() {
    return $this->db
                ->where('deleted', 0)
                ->order_by('RAND()')
                ->limit(1)
                ->get(USER_TABLE)
                ->row();
  }

  /**
   * Método que retorn as
   * informações de um determinado
   * mentor na tabela mentores
   *
   * @param (int) $id ID do mentor no banco
   * @return (obj) retorna os dados do mentor com o ID = $id no banco
   *
   */

  public function getMentor($id) {
    return $this->db
                ->where('id', $id)
                ->get(USER_TABLE)
                ->row();
  }

  /**
   * Método que retorn as
   * informações de um determinado
   * mentor na tabela mentores
   *
   * @param (int) $id ID do mentor no banco
   * @return (obj) retorna os dados do mentor com o ID = $id no banco
   *
   */

   public function getMentorEmail($email) {
    return $this->db
                ->where('email', $email)
                ->get(USER_TABLE)
                ->row();
  }

  /**
   * Método que retorn o nome das
   * imagens para determinado
   * mentor na tabela mentores
   *
   * @param (int) $id ID do mentor no banco
   * @return (obj) retorna as imagens do mento com o ID = $id no banco
   *
   */

  public function getMentorImg($id) {
    return $this->db
                ->select('imgGrande, imgModal, thumb')
                ->where('deleted', 0)
                ->where('id', $id)
                ->get(USER_TABLE)
                ->row();
  }

  /**
   * Método que retorna quantidade
   * de mentores na tabela mentores
   *
   * @return (int) retorna quantos mentores não deletados existem no banco
   *
   */
  public function countMentor() {
    $this->db
         ->where('deleted', 0)
         ->from(USER_TABLE);
		return $this->db->count_all_results();
	}

  /**
   * Método que insere informações
   * na tabela mentores
   *
   * @param (array) $data dados a serem inseridos
   * @return (int) retorna id que foi inserido no banco
   *
   */
  public function insert($data){
    $this->db->insert(USER_TABLE, $data);
    return $this->db->insert_id();
  }

  /**
   * Método que insere informações
   * na tabela interesses
   *
   * @param (array) $data dados a serem inseridos
   * @return (int) retorna id que foi inserido no banco
   *
   */
  public function insert_interesse($data){
    $this->db->insert(INTERESSES_TABLE, $data);
    return $this->db->insert_id();
  }


  /**
   * Método que edita informações
   * na tabela mentores
   *
   * @param (int) $id id do mentor no banco
   * @param (array) $data dados a serem editados
   * @return (int) retorna as linhas afetadas
   *
   */
  public function edit($id, $data){
		$this->db->where('id', $id);
		$this->db->update(USER_TABLE, $data);
	  return $this->db->affected_rows();
	}


  /**
   * Método que deleta informações
   * na tabela interesses
   *
   * @param (int) $id id do mentor no banco
   * @return (int) deleta os interesses do mentor
   *
   */
  public function deleteInteresse($id){
		$this->db->where('id_ref', $id);
		$this->db->delete(INTERESSES_TABLE);
	  return $this->db->affected_rows();
	}


  /**
   * Método que gera slug a
   * partir do nome do mentor
   *
   * @param (string) $nome nome do mentor
   * @return (string) retorna a slug a partir do nome do autor
   *
   */
  public function geraSlug($nome) {

    $jaExiste= true;
    $pos= 0;

    while($jaExiste) {
      $table = array(
        'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'
      );
      $slug = strtolower(strtr($nome, $table));
      $slug = preg_replace('#[^0-9a-z]+#i', " ", $slug);
      $slug = explode(' ', $slug);

      $slug = substr($slug[0], 0, 1) . '.'. $slug[1] .'@dm9';

      if($pos>0) {
        $slug = $slug.'-'.$pos;
      }

      $resultados =
                    $this->db
                         ->where('status', 1)
                         ->where('usuario', $slug)
                         ->get(ADMIN_TABLE)
                         ->num_rows();

      if ($resultados > 0) {
         $jaExiste= true;
         $pos++;
      } else {
        $jaExiste= false;
      }

    }

    return strtolower($slug);

  }
}
