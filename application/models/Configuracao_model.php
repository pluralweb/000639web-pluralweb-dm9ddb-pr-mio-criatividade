<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracao_model extends CI_Model {

  public function getDatas(){
    $this->db->select('id, inicio, fim');
    return $this->db->get(DATAS_TABLE)->result();
  }

  public function isValid(){
    $datas = $this->getDatas();
    $now = strtotime(date('Y-m-d'));
    $returnVal = array(
                        'valid' => false,
                        'begin' => false,
                        'end' => false
                      );

    foreach ($datas as $data) {
      $begin = strtotime($data->inicio);
      $end = strtotime($data->fim);

      if($now >= $begin && $now <= $end){
        $returnVal['valid'] = true;
        $returnVal['begin'] = $data->inicio;
        $returnVal['end'] = $data->fim;
        break;
      }
    }

    return $returnVal;

  }

  public function insertData($data){
    $this->db->insert(DATAS_TABLE, $data);
    return $this->db->insert_id();
  }

  public function insertMes($data){
    $this->db->insert(MESES_TABLE, $data);
    return $this->db->insert_id();
  }

  public function deleteMes($id){
    $this->db->where('id', $id);
    $this->db->delete(MESES_TABLE);
  }

  public function deleteData($id){
    $this->db->where('id', $id);
    $this->db->delete(DATAS_TABLE);
  }

  public function getMeses(){
    $this->db->select('id, inicio, fim');
    return $this->db->get(MESES_TABLE)->result();
  }

}
