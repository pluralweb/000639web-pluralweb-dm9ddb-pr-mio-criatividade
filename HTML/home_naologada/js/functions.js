$(document).ready(function() {
    
    
    /**
     * Show tabs from logged user
     */

    $("section.logged nav .nav-item").click(function() {
        var target = $(this).data("target");
        if (!$("div." + target).is(":visible")) {
            $("section.logged .content").hide();
            $("section.logged ." + target).fadeIn(function() {
                $('.scroll-pane').data('jsp').reinitialise();
            });
            $("section.logged nav .nav-item").removeClass("active");
            $(this).addClass("active");
            $("section.logged div.cadastre-ideia").removeClass("d_none");
            var prop = (target == "regulamento") ? true : false;
            $("section.logged nav .slider").prop("checked", prop);
            $(".ideiadomes").css('padding-top', "0px");
        }
    });

    /**
     * Show tabs from logged user when checkbox is marked
     */
    $("section.logged nav .slider").change(function() {
        var check = $(this).is(":checked");
        $("section.logged .content").hide();
        $("section.logged nav .nav-item").removeClass("active");
        var target = "cadastre-ideia";
        if (check) {
            target = "regulamento";
            $(".ideiadomes").css('padding-top', "76px");
        } else {
            $(".ideiadomes").css('padding-top', "0px");
        }
        $("section.logged ." + target).fadeIn(function() {
            $('.scroll-pane').data('jsp').reinitialise();
        });
        $("section.logged nav .nav-item[data-target=" + target + "]").addClass("active");

    });

    /**
     * Show text from shortlist
     */

    $("section.shortlist .shortlist-titles button").click(function() {
        var target = $(this).data("target");
        $("section.shortlist .shortlist-titles button").removeClass("active");
        $(this).addClass("active");

        $("section.shortlist article").hide();
        $("section.shortlist article." + target).fadeIn(function() {
            if ($(window).width() < 768) {
                $("html, body").animate({
                    scrollTop: $(".sh-title:visible").offset().top - 20
                }, "900");
            }
        });
    });

    /**
     * Anchor to top
     */
     $(".backtotop").click(function() {
         $('html,body').animate({ scrollTop: 0 }, 'slow');
     });


    /**
     * anchor to ideia do mes
     */
    $(".anchor").click(function() {
        $('html,body').animate({ scrollTop: $('.ideiadomes').offset().top - 100 }, 'slow');
    });


    /**
     * Login from page
     */

    $('.doLogin').on('click', function(e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, "900", function() {
            $('.login input[type="email"]').focus();
        });
        return false;
    })


    /**
     * anchor to top
     */
    $('.backtotop').on('click', function() {
        scrollToTop();
    });

    function scrollToTop(elementName) {
        if (elementName) {
            $('html,body').animate({ scrollTop: $(elementName).offset().top }, 'slow');
        } else {
            $('html,body').animate({ scrollTop: 0 }, 'slow');
        }

    }

    $('#login').on('submit', function(e) {
        e.preventDefault();
        var el = $(this);
        var email = el.find('.email');
        var emailVal = email.val().trim();
        var erros = 0;
        var errorMsg = el.find('.errormsg');

        if (emailVal.length == 0) {
            ++erros;
            email.addClass('error');
            errorMsg.fadeIn();
        } else if (!validaEmail(emailVal)) {
            ++erros;
            email.addClass('error');
            errorMsg.fadeIn();
        } else if (emailVal.indexOf('dm9ddb.com.br') == -1) {
            ++erros;
            email.addClass('error');
            errorMsg.fadeIn();
        } else {
            email.removeClass('error');
            errorMsg.fadeOut();

            $.post(url + 'home/login', {
                email: emailVal
            }, function(data) {
                console.log();
                if (data.erro == 1) {
                    email.addClass('error');
                    errorMsg.fadeIn();
                } else {
                    email.removeClass('error');
                    errorMsg.fadeOut();
                    location.href = url + 'criatividade';
                }
            }, 'json');
        }

        return false;
    })

    $('#campos').on('submit', function(e) {
        e.preventDefault();
        var el = $(this);
        var titulo = el.find('[name="titulo"]');
        var ideia = el.find('[name="ideia"]');
        var contr = el.find('[name="contr"]');
        var part = el.find('[name="part"]');
        var motivo = el.find('[name="motivo"]');
        var tituloVal = titulo.val().trim();
        var ideiaVal = ideia.val().trim();
        var contrVal = contr.val().trim();
        var partVal = part.val().trim();
        var motivoVal = motivo.val().trim();

        var erros = 0;

        var errorMsg = el.find('.errormsg');

        if (tituloVal.length == 0) {
            ++erros;
            titulo.closest('div').addClass('error');
        } else {
            titulo.closest('div').removeClass('error');
        }
        if (ideiaVal.length == 0) {
            ++erros;
            ideia.closest('div').addClass('error');
        } else {
            ideia.closest('div').removeClass('error');
        }
        if (contrVal.length == 0) {
            ++erros;
            contr.closest('div').addClass('error');
        } else {
            contr.closest('div').removeClass('error');
        }
        if (partVal.length == 0) {
            ++erros;
            part.closest('div').addClass('error');
        } else {
            part.closest('div').removeClass('error');
        }
        if (motivoVal.length == 0) {
            ++erros;
            motivo.closest('div').addClass('error');
        } else {
            motivo.closest('div').removeClass('error');

        }
        if (erros == 0) {
            errorMsg.fadeOut();

            errorMsg.clone().insertAfter(errorMsg).addClass('returnMsg');
            var returnMsg = $('.returnMsg');

            $.post(url + 'home/saveCampos',
                el.serialize(),
                function(data) {
                    if (data.erro == 0) {
                        el.get(0).reset();
                        $('.hideForm').fadeOut(function() {
                            $('.showSuccess').fadeIn()
                        })
                    } else {
                        returnMsg.text(data.msg).fadeIn();
                        setTimeout(function() {
                            returnMsg.fadeOut();
                        }, 3000)
                    }

                }, 'json');
        } else {
            errorMsg.fadeIn();
        }
        return false;
    })
    $('.showSuccess').on('click', function(e) {
        e.preventDefault();
        $('.showSuccess').fadeOut(function() {
            $('.hideForm').fadeIn()
        })
        return false;
    })
});

function validaEmail(email) {
    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
    if (email == '' || !re.test(email)) {
        return false
    } else {
        return true;
    }
}
